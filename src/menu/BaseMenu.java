package menu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import lowlevel.MarbleFilter;
import lowlevel.Methods;
import lowlevel.SwimFilter;
import lowlevel.TransperencyFilter;
import sound.MusicPlayer;
import sound.Sound;

public class BaseMenu extends JPanel implements Runnable, MouseListener {
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();
	private static final long serialVersionUID = -890522268916356500L;
	MenuObject[] objects = new MenuObject[4];
	SubMenu[] submenus = new SubMenu[4];

	Color[] colors = { new Color(168, 27, 73), new Color(44, 180, 241), new Color(179, 157, 207),
			new Color(215, 228, 189) };

	Color[] backgrounds = { new Color(84, 13, 36), new Color(22, 90, 120), new Color(89, 78, 103),
			new Color(185, 218, 159) };

	ArrayList<Sound> sounds = MusicPlayer.getMenuMusic();
	MusicPlayer sound = new MusicPlayer(sounds.get(0));

	BufferedImage backImage;
	MenuObject back, tree;

	String[] items = { "Quit", "Options", "Create", "Play" };

	int selected = -1, mouseOver = -1;

	Color backTo = Color.WHITE;

	int fps = 0;
	long lastUpdate = 0;
	int counter = 0;

	public BaseMenu() {
		setLayout(null);

		setBackground(Color.WHITE);
		addMouseListener(this);

		MarbleFilter filter = new MarbleFilter();

		filter.setXScale(10);
		filter.setYScale(10);

		submenus[0] = new QuitMenu(this);
		submenus[1] = new OptionsMenu(this);
		submenus[2] = new CreateMenu(this);
		submenus[3] = new PlayMenu(this);

		// sound.setMusicVolume(.3);

		try {
			back = new MenuObject();
			backImage = ImageIO.read(new File("Resources/Images/Menu/BACK.png"));
			back.image = /* filter.filter( */backImage/* , null) */;
			back.x = SCREEN.width;
			back.y = SCREEN.height;
			back.yTo = -100;
			back.xTo = -100;
			back.width = 0;
			back.height = 0;
			back.widthTo = 100;
			back.heightTo = 100;
			back.rotationTo = 3 * Math.PI / 4;

			tree = new MenuObject();
			BufferedImage coconut = ImageIO.read(new File("Resources/Images/Menu/COCONUT.png"));
			tree.image = filter.filter(new TransperencyFilter().filter(coconut, new BufferedImage(coconut.getWidth(),
					coconut.getHeight(), BufferedImage.TYPE_INT_ARGB)), null);
			// tree.image = filter.filter(coconut, null);
			tree.x = SCREEN.width;
			tree.y = SCREEN.height;
			tree.width = 0;
			tree.height = 0;
			tree.rotation = Math.PI;

			tree.xTo = 100 + (int) (SCREEN.width * .35);
			tree.yTo = SCREEN.height / 2;
			tree.heightTo = SCREEN.height - 40;
			tree.widthTo = (int) (SCREEN.width * .7);
			tree.rotationTo = 0;

			int x = SCREEN.width / 2 - 200, y = SCREEN.height - 200;
			for (int i = 0; i < 4; i++) {
				objects[i] = new MenuObject();
				objects[i].x = -0;
				objects[i].y = (i + 1) * SCREEN.height / 4;
				objects[i].image = ImageIO.read(new File("Resources/Images/Menu/FACE" + (i + 1) + ".png"));
				objects[i].rotation = -Math.PI / 2;
				objects[i].width = 0;
				objects[i].height = 0;

				objects[i].xTo = x;
				objects[i].yTo = y;
				objects[i].rotationTo = Math.PI / 4;
				objects[i].widthTo = 150 + 40 * (i + 1);

				x += 150 + 35 * (i + 1);
				y -= 40 + 8.2 * (i + 1);

				submenus[i].reposition(SCREEN.width / 4 + 10, 30 - SCREEN.height, 3 * SCREEN.width / 4 - 20,
						SCREEN.height - 40);

				add(submenus[i]);
			}
		} catch (Exception e) {
		}

		new Thread(this).start();
	}

	public void run() {
		final double SPEED = 5;

		SwimFilter filter = new SwimFilter();
		filter.setAmount(10);

		// float time = 0f;

		while (true) {
			try {
				Thread.sleep(15);
			} catch (Exception e) {
			}

			counter++;
			if (counter % 10 == 0) {
				counter %= 10;
				if (System.currentTimeMillis() - lastUpdate == 0)
					fps = 1000;
				else
					fps = (int) (10000 / (System.currentTimeMillis() - lastUpdate));
				lastUpdate = System.currentTimeMillis();
			}

			// time += .05f;
			// filter.setTime(time);
			// filter.filter(backImage, back.image);

			mouseOver = -1;

			for (int i = 0; i < 4; i++) {
				Point mouse = Methods.mouse();
				if ((mouse.x - objects[i].x) * (mouse.x - objects[i].x) + (mouse.y - objects[i].y)
						* (mouse.y - objects[i].y) < objects[i].width * objects[i].width / 4)
					mouseOver = i;
			}
			setBackground(Methods.colorMeld(getBackground(), backTo, .2));

			if (Math.abs(getBackground().getRed() - backTo.getRed()) < 10
					&& Math.abs(getBackground().getGreen() - backTo.getGreen()) < 10
					&& Math.abs(getBackground().getBlue() - backTo.getBlue()) < 10)
				setBackground(new Color(backTo.getRed(), backTo.getGreen(), backTo.getBlue()));

			tree.update(SPEED);
			back.update(SPEED);

			for (int i = 0; i < 4; i++) {
				objects[i].update(SPEED);
				submenus[i].update(SPEED);

				if (selected == -1) {
					if (i == mouseOver) {
						objects[i].rotationTo += .05 * ((i + 1) % 2 == 0 ? 1 : -1);
						objects[i].widthTo = 200 + 40 * (i + 1);
						objects[i].heightTo = 200 + 40 * (i + 1);
					} else {
						objects[i].rotationTo = Math.PI / 4;
						objects[i].widthTo = 150 + 40 * (i + 1);
						objects[i].heightTo = 150 + 40 * (i + 1);
					}
				}
			}
			repaint();
		}
	}

	public void paintComponent(Graphics asdffdsa) {
		Graphics2D g = (Graphics2D) asdffdsa;
		super.paintComponent(g);

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.setFont(new Font("SERIF", Font.BOLD, 25));
		g.setColor(new Color(255 - getBackground().getRed(), 255 - getBackground().getGreen(), 255 - getBackground()
				.getBlue()));
		g.drawString("" + fps, 20, 40);

		tree.draw(g);
		back.draw(g);

		g.setFont(new Font("SERIF", Font.BOLD, 39));

		for (int i = 0; i < 4; i++) {
			g.translate(objects[i].x, objects[i].y);
			g.rotate(objects[i].rotation);
			g.drawImage(objects[i].image, -(int) objects[i].width / 2, -(int) objects[i].height / 2,
					(int) objects[i].width, (int) objects[i].height, this);
			g.rotate(-objects[i].rotation);

			g.setColor(Color.BLACK);
			g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i]) / 2 - 1, (int) objects[i].height / 2 + 34);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2 - 1, (int) objects[i].height / 2 + 36);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2 + 1, (int) objects[i].height / 2 + 34);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2 + 1, (int) objects[i].height / 2 + 36);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2, (int) objects[i].height / 2 + 34);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2, (int) objects[i].height / 2 + 36);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2 + 1, (int) objects[i].height / 2 + 35);
			// g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i])
			// / 2 - 1, (int) objects[i].height / 2 + 35);

			g.setColor(colors[i]);// Methods.randomColor((i + 1) * 84 - 40, 255)
			g.drawString(items[i], -g.getFontMetrics().stringWidth(items[i]) / 2, (int) objects[i].height / 2 + 35);

			g.translate(-objects[i].x, -objects[i].y);
		}
	}

	public void resetMenu() {
		back.yTo = -100;
		back.xTo = -100;
		back.widthTo = 100;
		back.heightTo = 100;
		back.rotationTo = 3 * Math.PI / 4;

		tree.xTo = 100 + (int) (SCREEN.width * .35);
		tree.yTo = SCREEN.height / 2;
		tree.heightTo = SCREEN.height - 40;
		tree.widthTo = (int) (SCREEN.width * .7);
		tree.rotationTo = 0;

		int x = SCREEN.width / 2 - 200, y = SCREEN.height - 200;
		for (int i = 0; i < 4; i++) {
			objects[i].xTo = x;
			objects[i].yTo = y;
			objects[i].rotationTo = Math.PI / 4;
			objects[i].heightTo = 150 + 40 * (i + 1);
			objects[i].widthTo = 150 + 40 * (i + 1);

			x += 150 + 35 * (i + 1);
			y -= 40 + 8.2 * (i + 1);

			submenus[i].xTo = SCREEN.width / 4 + 10;
			submenus[i].yTo = 30 - SCREEN.height;
			submenus[i].widthTo = 3 * SCREEN.width / 4 - 20;
			submenus[i].heightTo = SCREEN.height - 40;

			submenus[i].setOnScreen(false);

		}

		mouseOver = -1;
		selected = -1;
		backTo = Color.WHITE;
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent evt) {

	}

	public void move(int q) {
		if (q == 0)
			resetMenu();
		else if (q < 10) {
			q -= 1;
			split(q);

			objects[q].xTo = SCREEN.width / 8;
			objects[q].yTo = SCREEN.height / 2;
			objects[q].widthTo = 2 * SCREEN.width / 8 - 10;
			objects[q].rotationTo = 2 * Math.PI;
			objects[q].heightTo = 2 * SCREEN.width / 8 - 10;

			submenus[q].yTo = 20;

			backTo = backgrounds[q];

			back.xTo = SCREEN.width / 8;
			back.yTo = 7 * SCREEN.height / 8;
			back.widthTo = 147;
			back.heightTo = 142;
			back.rotationTo = 2 * Math.PI;

			submenus[q].setOnScreen(true);
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (back.contains(evt.getPoint())) {
			sound.playSoundEffect("Resources/Music/Effects/MENU_TRANSITION2.wav");
			selected /= 10;
			move(selected);
		}

		if (mouseOver != -1 && selected == -1) {
			sound.playSoundEffect("Resources/Music/Effects/MENU_TRANSITION2.wav");
			selected = (mouseOver + 1);
			move(selected);
		}
	}

	public void split(int s) {
		for (int i = 0; i < 4; i++) {
			if (s == i)
				continue;

			objects[i].xTo = 3 * SCREEN.width / 4 * Math.cos(2 * i * Math.PI / 5) + SCREEN.width / 2;
			objects[i].widthTo = 100;
			objects[i].heightTo = 100;
			objects[i].rotationTo = -Math.PI;
			objects[i].yTo = 3 * SCREEN.height / 4 * Math.sin(2 * i * Math.PI / 5) + SCREEN.height / 2;

		}

		tree.xTo = 3 * SCREEN.width / 4 + SCREEN.width / 2;
		tree.rotationTo = Math.PI;
		tree.yTo = SCREEN.height / 2;
		//
		// tree.rotationTo = -Math.PI;
		// tree.xTo = 4 * SCREEN.width / 3;
		// tree.yTo = 4 * SCREEN.height / 3;
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("MENU TEST");
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);

		BaseMenu b = new BaseMenu();
		frame.add(b, BorderLayout.CENTER);

		frame.setVisible(true);
	}
}
