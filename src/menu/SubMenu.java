package menu;

import javax.swing.JPanel;

public abstract class SubMenu extends JPanel {
	private static final long serialVersionUID = 5508374870920845454L;
	int xTo, yTo, widthTo, heightTo;

	protected boolean seen = false;
	
	BaseMenu parent;

	public SubMenu(BaseMenu b) {
		setOpaque(false);
		this.parent = b;
	}

	public void update(double speed) {
		// if (Math.abs(getX() - xTo) < speed)
		// setLocation(xTo, getY());
		// if (Math.abs(getY() - yTo) < speed)
		// setLocation(getX(), yTo);
		// if (Math.abs(getWidth() - widthTo) < speed)
		// setSize(widthTo, getHeight());
		// if (Math.abs(getHeight() - heightTo) < speed)
		// setSize(getWidth(), heightTo);

		if (getX() != xTo || getY() != yTo || getWidth() != widthTo || getHeight() != heightTo) {
			setBounds((int) (getX() + (xTo - getX()) / speed), (int) (getY() + (yTo - getY()) / speed),
					(int) (getWidth() + (widthTo - getWidth()) / speed), (int) (getHeight() + (heightTo - getHeight())
							/ speed));
		}
	}

	public boolean isOnScreen() {
		return seen;
	}

	public void setOnScreen(boolean b) {
		seen = b;
	}

	public void reposition(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);
		xTo = a;
		yTo = b;
		widthTo = c;
		heightTo = d;
	}

}
