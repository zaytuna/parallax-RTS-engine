package menu;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import lowlevel.Methods;

public class MenuObject {
	BufferedImage image;
	double rotation, rotationTo, x, y, width, height, xTo, yTo, widthTo, heightTo;

	public boolean contains(java.awt.Point p) {
		return Math.abs(Methods.getRotateX(p.x - x, p.y - y, rotation)) < width / 2
				&& Math.abs(Methods.getRotateY(p.x - x, p.y - y, rotation)) < height / 2;
	}

	public void update(double speed) {
		x += (xTo - x) / speed;
		y += (yTo - y) / speed;
		width += (widthTo - width) / speed;
		height += (heightTo - height) / speed;
		rotation += (rotationTo - rotation) / speed;

		width = Math.max(0, width);
		height = Math.max(0, height);
	}

	public void draw(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.translate(x, y);
		g.rotate(rotation);
		g.drawImage(image, -(int) width / 2, -(int) height / 2, (int) width, (int) height, null);
		g.rotate(-rotation);
		g.translate(-x, -y);
	}

	public void set(double a, double b, double c, double d) {
		x = a;
		y = b;
		width = c;
		height = d;
	}

	public void move(double a, double b, double c, double d) {
		set(a, b, c, d);
		to(a, b, c, d);
	}

	public void to(double a, double b, double c, double d) {
		xTo = a;
		yTo = b;
		widthTo = c;
		heightTo = d;
	}
}
