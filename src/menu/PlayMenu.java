package menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import lowlevel.TransperencyFilter;

public class PlayMenu extends SubMenu implements MouseListener {
	private static final long serialVersionUID = -2198087849316909775L;

	public static final String[] SINGLE_PLAYER_OBJECT_NAMES = { "TUTORIAL.png", "ROCK.png", "MOUSE.png", "CRAYON.png",
			"LOAD.png", "Freeform.png" };

	MenuObject single = new MenuObject(), multi = new MenuObject();

	ArrayList<MenuObject> singlePlayer = new ArrayList<MenuObject>();

	public PlayMenu(BaseMenu b) {
		super(b);

		addMouseListener(this);
		try {
			for (int i = 0; i < 6; i++) {
				MenuObject o = new MenuObject();
				o.image = ImageIO.read(new File("Resources/Images/Menu/Play/" + SINGLE_PLAYER_OBJECT_NAMES[i]));
				o.move(0, 0, 0, 0);

				singlePlayer.add(o);
			}

			multi.image = new TransperencyFilter().filter(ImageIO.read(new File(
					"Resources/Images/Menu/Play/MULTIPLAYER_COMPLETE.png")), null);
			single.image = new TransperencyFilter().filter(ImageIO.read(new File(
					"Resources/Images/Menu/Play/SINGLEPLAYER_COMPLETE.png")), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setOnScreen(boolean b) {
		super.setOnScreen(b);

		if (b) {
			parent.objects[3].xTo = BaseMenu.SCREEN.width / 8;

			widthTo = 3 * BaseMenu.SCREEN.width / 4 - 20;
			parent.back.xTo = BaseMenu.SCREEN.width / 8;
			xTo = BaseMenu.SCREEN.width / 4 + 10;

			single.to(widthTo / 4, 3 * heightTo / 4, widthTo / 2, heightTo / 3);
			multi.to(2 * widthTo / 3, heightTo / 4, 3 * widthTo / 4, heightTo / 2);

			for (int i = 0; i < 6; i++) {
				singlePlayer.get(i).to(single.xTo, single.yTo, -10, -10);
				singlePlayer.get(i).rotationTo = Math.PI * (((i % 2) * 2) - 1);
			}
		} else {
			single.to(4 * getWidth() / 3, 3 * getHeight() / 4, getWidth() / 2, getHeight() / 3);
			multi.to(-getWidth() / 2, getHeight() / 4, 3 * getWidth() / 4, getHeight() / 2);
		}
	}

	@Override
	public void update(double speed) {
		super.update(speed);

		single.update(speed);
		multi.update(speed);

		for (int i = 0; i < 6; i++)
			singlePlayer.get(i).update(speed);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		single.draw(g2d);
		multi.draw(g2d);

		for (int i = 0; i < 6; i++)
			singlePlayer.get(i).draw(g2d);
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (single.contains(evt.getPoint())) {
			parent.selected = 41;
			parent.backTo = new Color(255, 255, 204);

			parent.sound.playSoundEffect("Resources/Music/Effects/MENU_TRANSITION2.wav");

			single.widthTo = -80;
			multi.heightTo = -80;
			single.xTo = 0;
			multi.yTo = 0;

			parent.objects[3].xTo = -BaseMenu.SCREEN.width / 8;
			parent.back.xTo = BaseMenu.SCREEN.width / 2;
			parent.back.rotationTo = 0;
			xTo = 0;
			widthTo = BaseMenu.SCREEN.width;

			singlePlayer.get(0).to(BaseMenu.SCREEN.width / 6, 9 * getHeight() / 10, BaseMenu.SCREEN.width / 3,
					getHeight() / 5);// tutorial
			singlePlayer.get(1).to(BaseMenu.SCREEN.width / 6, getHeight() / 4, 350, 275);// rock
			singlePlayer.get(2).to(BaseMenu.SCREEN.width / 2, getHeight() / 2, 600, 200);// mouse
			singlePlayer.get(3).to(5 * BaseMenu.SCREEN.width / 6, 3 * getHeight() / 4, 350, 330);// crayon
			singlePlayer.get(4).to(2 * BaseMenu.SCREEN.width / 3, 90, BaseMenu.SCREEN.width / 3 + 50, 230);// load
			singlePlayer.get(5).to(5 * BaseMenu.SCREEN.width / 6, 210, BaseMenu.SCREEN.width / 3 + 50, 230);// freeform

			for (int i = 0; i < 6; i++)
				singlePlayer.get(i).rotationTo = 0;
		} else if (parent.back.contains(new Point(evt.getX() - getX(), evt.getY() - getY()))) {
			parent.sound.playSoundEffect("Resources/Music/Effects/MENU_TRANSITION2.wav");
			parent.selected /= 10;
			parent.move(parent.selected);

		} else if (multi.contains(evt.getPoint())) {
			parent.sound.playSoundEffect("Resources/Music/Effects/MENU_TRANSITION2.wav");
			parent.selected = 41;
			parent.backTo = Color.BLACK;

			single.heightTo = -80;
			multi.widthTo = -80;
			single.yTo = getHeight();
			multi.xTo = getWidth();
		} else if (singlePlayer.get(0).contains(evt.getPoint())) {
			// TUTORIAL
		} else if (singlePlayer.get(1).contains(evt.getPoint())) {
			// ROCK MISSION
		} else if (singlePlayer.get(2).contains(evt.getPoint())) {
			// MOUSE MISSION
		} else if (singlePlayer.get(3).contains(evt.getPoint())) {
			// CRAYON MISSION
		} else if (singlePlayer.get(4).contains(evt.getPoint())) {
			// LOAD
		} else if (singlePlayer.get(5).contains(evt.getPoint())) {
			// Next menu
		}
	}
}
