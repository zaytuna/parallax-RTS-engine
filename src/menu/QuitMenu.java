package menu;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class QuitMenu extends SubMenu {
	private static final long serialVersionUID = -5191753728677272573L;

	public QuitMenu(BaseMenu b) {
		super(b);
		JLabel l = new JLabel(new ImageIcon("Resources/Images/Menu/Exit/EXIT.png"));
		setLayout(new BorderLayout());
		add(l, BorderLayout.CENTER);

		l.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				if (evt.getX() > 360 && evt.getX() < 560 && evt.getY() > 600 && evt.getY() < 700)
					System.exit(0);
			}
		});
	}

}
