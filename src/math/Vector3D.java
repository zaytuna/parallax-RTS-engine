package math;

import lowlevel.Methods;

public class Vector3D {
	public double x, y, z;

	public Vector3D(double a, double b, double c) {
		x = a;
		y = b;
		z = c;
	}

	public void set(double a, double b, double c) {
		x = a;
		y = b;
		z = c;
	}

	public void minus(Vector3D other) {
		this.x -= other.x;
		this.y -= other.y;
		this.z -= other.z;
	}

	public double magnitude() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	public double getHorizontalAngle() {
		return Math.atan2(y, x);
	}

	public void unitize() {
		double m = magnitude();
		z /= m;
		x /= m;
		y /= m;
	}

	public double getVerticalAngle() {
		return Math.acos(z / magnitude());
	}

	public void negate() {
		x *= -1;
		y *= -1;
		z *= -1;
	}

	public double dotProduct(Vector3D other) {
		return x * other.x + y * other.y + z * other.z;
	}

	public double angleBetween(Vector3D other) {
		return Math.acos(dotProduct(other) / (magnitude() * other.magnitude()));
	}

	public void add(Vector3D p) {
		x += p.x;
		y += p.y;
		z += p.z;
	}

	public static Vector3D getSum(Vector3D p1, Vector3D p2) {
		return new Vector3D(p1.x + p2.x, p2.y + p1.y, p2.z + p1.z);
	}

	public double getInclinationAngle() {
		return Math.acos(z / getDistanceFromOrigin());
	}

	public Vector3D getLocation() {
		return this;
	}

	public double getDistanceFromOrigin() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	public double getDirectionalMagnitude(Vector3D positive) {
		if ((positive.x != 0 && positive.x * x < 0)
				|| (positive.y != 0 && positive.y * y < 0)
				|| (positive.z != 0 && positive.z * z < 0))
			return -1*magnitude();
		return magnitude();
	}

	public double distanceFrom(Vector3D p) {
		return Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)
				+ (z - p.z) * (z - p.z));
	}

	public Vector3D scale(double scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return this;
	}

	public Vector3D clone() {
		return new Vector3D(x, y, z);
	}

	public boolean equals(Vector3D other) {
		return (x == other.x) && (y == other.y) && (z == other.z);
	}

	public String toString() {
		return "Vector3D(" + x + "," + y + "," + z + ")";
	}

	public void incrementAngle(double alpha) {
		double a = Methods.getRotateX(x, y, alpha);
		double b = Methods.getRotateY(x, y, alpha);
		x = a;
		y = b;
	}

	public void incrementVerticalAngle(double beta) {
		double c = Methods.getRotateY(Math.sqrt(x * x + y * y), z, beta);
		double b = Methods.getRotateX(y, z, beta);
		double a = Methods.getRotateX(x, z, beta);

		x = a;
		y = b;
		z = c;
	}

	public static Vector3D subtract(Vector3D v1, Vector3D v2) {
		Vector3D n = v1.clone();
		n.minus(v2);
		return n;
	}

	public double get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			return 0;
		}
	}

	public Vector3D cross(Vector3D other) {
		return new Vector3D(y * other.z - z * other.y, z * other.x - x
				* other.z, x * other.y - y * other.x);
	}

	public static Vector3D findUp(double a, double b, double c) {
		Vector3D foreward = new Vector3D(Math.sin(a) * Math.cos(b), Math.cos(a)
				* Math.cos(b), Math.sin(b));
		Vector3D up = new Vector3D(Math.sin(a) * Math.cos(b + Math.PI / 2),
				Math.cos(a) * Math.cos(b + Math.PI / 2), Math.sin(b + Math.PI
						/ 2));

		up = Matrix.createRotationMatrixAbout(foreward, c).eval(up);
		up.unitize();

		return up;
	}

	public static Vector3D findRight(double a, double b, double c) {
		return findUp(a, b, c).cross(
				new Vector3D(Math.sin(a) * Math.cos(b), Math.cos(a)
						* Math.cos(b), Math.sin(b)));
	}
}
