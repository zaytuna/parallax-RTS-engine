package math;

public class Vector {
	int dimension;
	double[] values;

	public Vector(double... vals) {
		this.values = vals;
		dimension = vals.length;
	}

	public void set(double... values) {
		this.values = values;
		dimension = values.length;
	}

	public void minus(Vector other) {
		if (other.dimension != dimension)
			throw new IllegalArgumentException("Non-matching vector dimensions");

		for (int i = 0; i < values.length; i++)
			values[i] -= other.values[i];
	}

	public double magnitude() {
		double total = 0;
		for (int i = 0; i < values.length; i++)
			total += values[i] * values[i];
		return Math.sqrt(total);
	}

	public void unitize() {
		double m = magnitude();
		for (int i = 0; i < values.length; i++)
			values[i] /= m;
	}

	public void negate() {
		for (int i = 0; i < values.length; i++)
			values[i] *= -1;
	}

	public double dotProduct(Vector other) {
		if (other.dimension != dimension)
			throw new IllegalArgumentException("Non-matching vector dimensions");

		double product = 0;
		for (int i = 0; i < values.length; i++)
			product += other.values[i] * values[i];

		return product;
	}

	public double angleBetween(Vector other) {
		return Math.acos(dotProduct(other) / (magnitude() * other.magnitude()));
	}

	public void add(Vector p) {
		for (int i = 0; i < values.length; i++)
			values[i] += p.values[i];
	}

	public static Vector getSum(Vector p1, Vector p2) {
		if (p1.dimension != p2.dimension)
			throw new IllegalArgumentException("Non-matching vector dimensions");
		Vector sum = p1.clone();
		sum.add(p2);
		return sum;
	}

	public Vector getLocation() {
		return this;
	}

	public double getDistanceFromOrigin() {
		double total = 0;
		for (int i = 0; i < values.length; i++)
			total += values[i] * values[i];
		return Math.sqrt(total);
	}

	public double distanceFrom(Vector p) {
		if (p.dimension != dimension)
			throw new IllegalArgumentException("Non-matching vector dimensions");

		double total = 0;
		for (int i = 0; i < values.length; i++)
			total += (values[i] - p.values[i]) * (values[i] - p.values[i]);
		return Math.sqrt(total);
	}

	public Vector scale(double scalar) {
		for (int i = 0; i < values.length; i++)
			values[i] *= scalar;
		return this;
	}

	public Vector clone() {
		return new Vector(values);
	}

	public boolean equals(Vector other) {
		for (int i = 0; i < values.length; i++)
			if (get(i) != other.get(i))
				return false;
		return true;
	}

	public String toString() {
		String s = "";
		for (double d : values)
			s += d + ",";
		return "Vector (" + s.substring(0, s.length() - 1) + ")";
	}

	public static Vector subtract(Vector v1, Vector v2) {
		Vector n = v1.clone();
		n.minus(v2);
		return n;
	}

	public double get(int i) {
		return values[i];
	}
}
