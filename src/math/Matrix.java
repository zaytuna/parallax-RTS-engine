package math;

public class Matrix {
	double[][] values; // row, column
	int rows, columns;

	public Matrix(double[]... vals) {
		int max = 0;
		for (int i = 0; i < vals.length; i++) {
			if (vals[i].length > max)
				max = vals[i].length;
		}

		double[][] copy = new double[vals.length][max];
		for (int i = 0; i < copy.length; i++) {
			for (int j = 0; j < copy[i].length; j++) {
				if (vals[i].length <= j)
					copy[i][j] = 0;
				else
					copy[i][j] = vals[i][j];
			}
		}
		this.rows = vals.length;
		this.columns = max;

		this.values = copy;
	}

	public void set(int r, int c, double val) {
		values[r][c] = val;
	}

	public double get(int r, int c) {
		return values[r][c];
	}

	public Matrix add(Matrix m2) {
		if (rows != m2.rows || columns != m2.columns)
			throw new IllegalArgumentException(
					"Matricies are not of the same dimension");

		Matrix result = createZeroMatrix(rows, columns);

		for (int i = 0; i < rows; i++)
			for (int j = 0; j < m2.columns; j++)
				result.set(i, j, get(i, j) + m2.get(i, j));

		return result;
	}

	public Matrix sub(Matrix m2) {
		if (rows != m2.rows || columns != m2.columns)
			throw new IllegalArgumentException(
					"Matricies are not of the same dimension");

		Matrix result = createZeroMatrix(rows, columns);

		for (int i = 0; i < rows; i++)
			for (int j = 0; j < m2.columns; j++)
				result.set(i, j, get(i, j) - m2.get(i, j));

		return result;
	}

	public Matrix mult(double s) {
		Matrix result = createZeroMatrix(rows, columns);

		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				result.set(i, j, get(i, j) * s);

		return result;
	}

	public Matrix mult(Matrix m2) {
		if (columns != m2.rows)
			throw new IllegalArgumentException(
					"Matricies are not of the correct dimension for multiplication");

		Matrix result = createZeroMatrix(rows, m2.columns);

		for (int r = 0; r < rows; r++)
			for (int c = 0; c < m2.columns; c++) {
				double total = 0;
				for (int i = 0; i < columns; i++)
					total += get(r, i) * m2.get(i, c);

				result.set(r, c, total);
			}

		return result;
	}

	public String toString() {
		String s = "";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++)
				s += values[i][j] + " ";

			s += "\n";
		}

		return s;
	}

	public Vector eval(Vector v) {
		if (columns != v.dimension)
			throw new IllegalArgumentException("Not correctly formatted");

		double[] data = new double[rows];

		for (int r = 0; r < rows; r++) {
			double total = 0;
			for (int i = 0; i < columns; i++)
				total += get(r, i) * v.get(i);
			data[r] = total;
		}

		return new Vector(data);
	}

	public Vector3D eval(Vector3D vector) {
		if (columns != 3 || rows != 3)
			throw new IllegalArgumentException(
					"Incorrectly formatted Matrix for 3D vector");

		Vector3D result = new Vector3D(0, 0, 0);

		result.x = vector.x * get(0, 0) + vector.y * get(0, 1) + vector.z
				* get(0, 2);
		result.y = vector.x * get(1, 0) + vector.y * get(1, 1) + vector.z
				* get(1, 2);
		result.z = vector.x * get(2, 0) + vector.y * get(2, 1) + vector.z
				* get(2, 2);

		return result;
	}

	public boolean equals(Matrix m) {
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				if (m.get(i, j) != get(i, j))
					return false;
		return true;
	}

	public Matrix clone() {
		double[][] dataclone = new double[rows][columns];
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				dataclone[i][j] = values[i][j];

		return new Matrix(dataclone);
	}

	public static Matrix createZeroMatrix(int r, int c) {
		double[][] data = new double[r][c];

		for (int i = 0; i < r; i++)
			for (int j = 0; j < c; j++)
				data[i][j] = 0;

		return new Matrix(data);
	}

	public static Matrix createIdentityMatrix(int n) {
		Matrix m = createZeroMatrix(n, n);

		for (int i = 0; i < n; i++)
			m.set(i, i, 1);

		return m;
	}

	public static Matrix createRandomMatrix(int r, int c) {
		Matrix m = createZeroMatrix(r, c);

		for (int i = 0; i < r; i++)
			for (int j = 0; j < c; j++)
				m.set(i, j, Math.random());

		return m;
	}

	public static Matrix create3DRotationMatrix(double a, double b, double c) {
		double c3 = Math.cos(a), s3 = Math.sin(a), c2 = Math.cos(b), s2 = Math
				.sin(b), c1 = Math.cos(c), s1 = Math.sin(c);

		return new Matrix(new double[][] { { c2 * c3, -c2 * s3, s2 },
				{ s1 * s2 * c3 + c1 * s3, -s1 * s2 * s3 + c1 * c3, -s1 * c2 },
				{ -c1 * s2 * c3 + s1 * s3, c1 * s2 * s3 + s1 * c3, c1 * c2 } });
	}

	public static Matrix createCrossProductMatrix(Vector3D v) {
		v.unitize();
		return new Matrix(new double[][] { { 0, -v.z, v.y }, { v.z, 0, -v.x },
				{ -v.y, v.x, 0 } });
	}

	public static Matrix createRotationMatrixAbout(Vector3D v, double theta) {
		return create3DProjectionMatrix(v).add(
				createIdentityMatrix(3).sub(create3DProjectionMatrix(v)).mult(
						Math.cos(theta))).add(
				createCrossProductMatrix(v).mult(Math.sin(theta)));
	}

	public static Matrix create3DProjectionMatrix(Vector3D v) {
		v.unitize();
		return new Matrix(new double[][] { { v.x * v.x, v.y * v.x, v.z * v.x },
				{ v.x * v.y, v.y * v.y, v.z * v.y },
				{ v.x * v.z, v.y * v.z, v.z * v.z } });
	}

	public static Matrix create3DProjectionMatrix(Vector3D a, Vector3D b) {
		a.unitize();
		b.minus(a.clone().scale(b.dotProduct(a)));
		b.unitize();

		return create3DProjectionMatrix(a).add(create3DProjectionMatrix(b));
	}

	public static void main(String[] args) {
		System.out.println();
	}

	public static Matrix getSum(Matrix x, Matrix y) {
		Matrix m = x.clone();
		m.add(y);
		return m;
	}
	
	public static Matrix getDiff(Matrix x, Matrix y) {
		Matrix m = x.clone();
		m.sub(y);
		return m;
	}
	
	public static Matrix getProduct(Matrix x, Matrix y) {
		Matrix m = x.clone();
		m.mult(y);
		return m;
	}	
}
