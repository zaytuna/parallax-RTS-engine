package math;

import lowlevel.Methods;

public class Vector2D {
	public double x, y;

	public Vector2D(double a, double b) {
		x = a;
		y = b;
	}

	public void set(double a, double b) {
		x = a;
		y = b;
	}

	public void minus(Vector2D v2) {
		this.x -= v2.x;
		this.y -= v2.y;
	}

	public double magnitude() {
		return Math.sqrt(x * x + y * y);
	}

	public double getAngle() {
		return Math.atan2(y, x);
	}

	public void unitize() {
		double m = magnitude();
		x /= m;
		y /= m;
	}

	public void negate() {
		x *= -1;
		y *= -1;
	}

	public double dotProduct(Vector3D other) {
		return x * other.x + y * other.y;
	}

	public double angleBetween(Vector3D other) {
		return Math.acos(dotProduct(other) / (magnitude() * other.magnitude()));
	}

	public void add(Vector3D p) {
		x += p.x;
		y += p.y;
	}

	public static Vector3D getSum(Vector3D p1, Vector3D p2) {
		return new Vector3D(p1.x + p2.x, p2.y + p1.y, p2.z + p1.z);
	}

	public double getDistanceFromOrigin() {
		return Math.sqrt(x * x + y * y);
	}

	public double getDirectionalMagnitude(Vector3D positive) {
		if ((positive.x != 0 && positive.x * x < 0) || (positive.y != 0 && positive.y * y < 0))
			return -1 * magnitude();
		return magnitude();
	}

	public double distanceFrom(Vector3D p) {
		return Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
	}

	public Vector2D scale(double scalar) {
		x *= scalar;
		y *= scalar;
		return this;
	}

	public Vector2D clone() {
		return new Vector2D(x, y);
	}

	public boolean equals(Vector3D other) {
		return (x == other.x) && (y == other.y);
	}

	public String toString() {
		return "Vector2D(" + x + "," + y + ")";
	}

	public void incrementAngle(double alpha) {
		double a = Methods.getRotateX(x, y, alpha);
		double b = Methods.getRotateY(x, y, alpha);
		x = a;
		y = b;
	}

	public static Vector2D subtract(Vector2D v1, Vector2D v2) {
		Vector2D n = v1.clone();
		n.minus(v2);
		return n;
	}

	public double get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		default:
			return 0;
		}
	}

	public Vector2D cross() {
		return new Vector2D(y, -x);
	}
}
