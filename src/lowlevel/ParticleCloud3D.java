package lowlevel;

import java.awt.Color;

import math.Vector3D;
import framework.Effect;
import gui.GamePanel;

public class ParticleCloud3D implements Effect {
	public double[] x, y, z, vx, vy, vz;//, ax, ay, az;
	public int[] rgb;
	public Color color;
	public double cx, cy, cz, power;

	public ParticleCloud3D(int size, Color c, double power, double px, double py, double pz) {
		x = new double[size];
		y = new double[size];
		z = new double[size];
		vx = new double[size];
		vy = new double[size];
		vz = new double[size];
		rgb = new int[size];

		for (int i = 0; i < size; i++) {
			x[i] = px;
			y[i] = py;
			z[i] = pz;

			double alpha = Math.random() * 2 * Math.PI;
			double beta = Math.random() * Math.PI / 2;

			vy[i] = Math.random() * Math.sin(alpha) * Math.cos(beta) * power;
			vx[i] = Math.random() * Math.cos(alpha) * Math.cos(beta) * power;
			vz[i] = Math.random() * Math.sin(beta) * power;

			rgb[i] = c.getRGB();
		}

		this.power = power;
		this.color = c;
		cx = px;
		cy = py;
	}

	public void toCenter() {
		for (int i = 0; i < x.length; i++) {
			x[i] = cx;
			y[i] = cy;
			z[i] = cz;

			double alpha = Math.random() * 2 * Math.PI;
			double beta = Math.random() * Math.PI / 2;

			vy[i] = Math.random() * Math.sin(alpha) * Math.cos(beta) * power / 100;
			vx[i] = Math.random() * Math.cos(alpha) * Math.cos(beta) * power / 100;
			vz[i] = Math.random() * Math.sin(beta) * power / 100;

			rgb[i] = color.getRGB();
		}
	}

	public void update() {
		for (int i = 0; i < x.length; i++) {
			x[i] += vx[i] / 2;
			y[i] += vy[i] / 2;
			z[i] += vz[i] / 2;

			vz[i] -= .01;

//			ax[i] = (cx - x[i]) * Math.random() / 40;
//			ay[i] = (cy - y[i]) * Math.random() / 40;
//			az[i] = (cz - z[i]) * Math.random() / 40;

			vx[i] *= .990;
			vy[i] *= .990;
			vz[i] *= .990;

//			if (Math.abs(x[i]) - cx <= 10 && Math.abs(y[i] - cy) <= 10) {
//				ax[i] = power * vx[i] / Math.sqrt(vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]) / 5;
//				ay[i] = power * vy[i] / Math.sqrt(vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]) / 5;
//				az[i] = power * vz[i] / Math.sqrt(vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]) / 5;
//			}

//			x[i] -= Math.abs(Methods.pseudoRandom(i)) * y[i] / 30;
//			y[i] -= Math.abs(Methods.pseudoRandom(i)) * z[i] / 30;
//			z[i] -= Math.abs(Methods.pseudoRandom(i)) * x[i] / 30;
		}
	}

	@Override
	public void draw(PixelDrawer p, GamePanel gp) {
		p.draw(gp, this);
	}

	@Override
	public boolean isDead() {
		return false;
	}

	@Override
	public Vector3D getPosition() {
		return new Vector3D(cx, cy, cz);
	}
}
