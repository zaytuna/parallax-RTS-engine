package lowlevel;

import gui.GamePanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.util.Arrays;

import math.Vector2D;

public class PixelDrawer {

	private final int WIDTH, HEIGHT;
	private int[] pixels;
	private MemoryImageSource source;
	private Image img;

	int color = 0;

	public PixelDrawer(int width, int height, Component o) {
		WIDTH = width;
		HEIGHT = height;

		pixels = new int[width * height];
		source = new MemoryImageSource(width, height, pixels, 0, width);

		source.setAnimated(true);
		source.setFullBufferUpdates(true);
		img = o.createImage(source);
	}

	public void setColor(Color c) {
		color = (c.getAlpha() << 24) | (c.getRed() << 26) | (c.getGreen() << 8) | c.getBlue();
	}

	public double simpleSin(double x) {
		return x - x * x * x / 6 + x * x * x * x * x / 120 - x * x * x * x * x * x * x / 5040;
	}

	public Image getImage() {
		return img;
	}

	public void clear(int rgb) {
		Arrays.fill(pixels, rgb);
	}

	public void clear() {
		Arrays.fill(pixels, 0);
	}

	public void update() {
		source.newPixels(0, 0, WIDTH, HEIGHT);
	}

	public int[] getPixels() {
		return pixels;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	/**
	 *Draws an oval of size w = width, h = height centered at x,y
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */
	public void fillOval(int x, int y, int w, int h, int rgb) {
		for (int j = -h / 2; j < h / 2; j++) {// absolute y pos
			double r = h / 2 + ((j + h / 2) / (double) h) * (w / 2 - h / 2);
			for (int i = 0; r * r > i * i + j * j; i++) {// relative x pos
				fillPixel(i + x, j + y, rgb);
				fillPixel(x - i, j + y, rgb);
			}
		}
	}

	public void drawPolygon(Polygon p) {
		for (int i = 0; i < p.npoints; i++)
			drawLine(p.xpoints[i], p.ypoints[i], p.xpoints[(i + 1) % p.npoints], p.ypoints[(i + 1) % p.npoints], color);
	}

	public void fillPolygon(Polygon p, int rgb, boolean drawLines) {
		Rectangle bounds = p.getBounds();
		int[] crossing = new int[p.npoints];
		int last = 0;
		int index = 0;

		for (int j = bounds.y; j < bounds.height + bounds.y; j++) {
			index = 0;
			Arrays.fill(crossing, Integer.MAX_VALUE);
			last = p.npoints - 1;
			for (int i = 0; i < p.npoints; i++) {
				if (p.ypoints[i] < j && p.ypoints[last] >= j || p.ypoints[last] < j && p.ypoints[i] >= j) {
					crossing[index++] = (int) ((double) (j - p.ypoints[last])
							* (double) ((p.xpoints[i] - p.xpoints[last]) / (double) (p.ypoints[i] - p.ypoints[last])) + p.xpoints[last]);
				}

				last = i;
			}
			Methods.insertionSort(crossing, crossing.length);

			for (int i = 0; i < index; i += 2) {
				for (int z = crossing[i]; z <= crossing[i + 1]; z++) {
					/*
					 * int alpha = 255; int red = (int)(Math.random()*255); int
					 * blue = (int)(Math.random()*255); int green =
					 * (int)(Math.random()*255);
					 */
					try {
						fillPixel(z, j, rgb);
					} catch (Exception e) {
					}// (alpha << 24)|(red << 16)|(green << 8)|blue;
				}
			}
		}
		if (drawLines)
			for (int i = 0; i < p.npoints; i++) {
				drawLine(p.xpoints[i], p.ypoints[i], p.xpoints[(i + 1) % p.npoints], p.ypoints[(i + 1) % p.npoints]);
			}
	}

	public void fillRect(int x, int y, int w, int h) {
		fillRect(x, y, w, h, color);
	}

	public void fillOval(int x, int y, int w, int h) {
		fillOval(x, y, w, h, color);
	}

	public void fillRect(int x, int y, int w, int h, int rgb) {
		for (int i = x; i < x + w; i++) {
			for (int j = y; j < h + y; j++)
				fillPixel(i, j, rgb);
		}
	}

	public void drawRect(int x, int y, int w, int h, int rgb) {
		for (int i = x; i < x + w; i++) {
			fillPixel(i, y, rgb);
			fillPixel(i, (y + h), rgb);
		}

		for (int j = y; j < h + y; j++) {
			fillPixel(x, j, rgb);
			fillPixel((x + w), j, rgb);
		}
	}

	public void fillArc(int x, int y, int w, int h, double a1, double a2, int rgb) {
		// for (int j = -h / 2; j < h / 2; j++) {// absolute y pos
		// double r = h / 2 + ((j + h / 2) / (double) h) * (w / 2 - h / 2);
		// int i = 0;
		//
		// while (i * i + j * j < r * r
		// && Math.abs(j / Math.tan(a1)) > Math.abs(i)
		// && Math.abs(j / Math.tan(a2)) > Math.abs(i))
		// fillPixel(x + i++, y + j, rgb);
		//
		// i = 0;
		//
		// while (i * i + j * j < r * r
		// && Math.abs(j / Math.tan(a1)) > Math.abs(i)
		// && Math.abs(j / Math.tan(a2)) > Math.abs(i))
		// fillPixel(x + i--, y + j, rgb);
		//
		// }
		fillRect(x, y, w, h / 2, rgb);

		Color c = new Color(rgb);

		for (int i = 10; i > 0; i--) {
			drawRect(x - 10 + i, y - 10 + i, w + 2 * (10 - i) - 1, h + 2 * (10 - i) - 1, new Color(c.getRed(), c
					.getGreen(), c.getBlue(), c.getAlpha() * i / 10).getRGB());
		}
	}

	public void drawImage(BufferedImage img, int x, int y) {
		int[] rgb = img.getRGB(0, 0, img.getWidth(), img.getHeight(), new int[img.getWidth() * img.getHeight()], 0, img
				.getWidth());

		for (int i = x; i < x + img.getWidth(); i++) {
			for (int j = y; j < img.getHeight() + y; j++)
				if (i < WIDTH && i > 0 && j < HEIGHT && j > 0)
					fillPixel(i, j, rgb[(i - x) + img.getWidth() * (j - y)]);
		}
	}

	public void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int rgb) {
		// y1<y2<y3 (Sorted Points)

		if (!(y1 <= y2 && y2 <= y3)) {
			if (y1 <= y3 && y3 <= y2)
				drawTriangle(x1, y1, x3, y3, x2, y2, rgb);
			else if (y2 <= y1 && y1 <= y3)
				drawTriangle(x2, y2, x1, y1, x3, y3, rgb);
			else if (y2 <= y3 && y3 <= y1)
				drawTriangle(x2, y2, x3, y3, x1, y1, rgb);
			else if (y3 <= y1 && y1 <= y2)
				drawTriangle(x3, y3, x1, y1, x2, y2, rgb);
			else if (y3 <= y2 && y2 <= y1)
				drawTriangle(x3, y3, x2, y2, x1, y1, rgb);

			return;
		}

		for (int y = y1; y < y2; y++) {
			int a = ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;

			for (int x = Math.min(b, a); x <= Math.max(b, a); x++)
				fillPixel(x, y, rgb);
		}
		for (int y = y2; y < y3; y++) {
			int a = ((y - y2) * (x3 - x2)) / (y3 - y2) + x2;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;

			for (int x = Math.min(b, a); x <= Math.max(b, a); x++)
				fillPixel(x, y, rgb);
		}
	}

	public void drawGouraudTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int rgb1, int rgb2, int rgb3) {
		drawGouraudTriangle(x1, y1, x2, y2, x3, y3, rgb1, rgb2, rgb3, pixels, WIDTH, HEIGHT);
	}

	public static void drawGouraudTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int rgb1, int rgb2,
			int rgb3, int[] pixels, int width, int height) {
		// y1<y2<y3 (Sorted Points)
		if (!(y1 <= y2 && y2 <= y3)) {
			if (y1 <= y3 && y3 <= y2)
				drawGouraudTriangle(x1, y1, x3, y3, x2, y2, rgb1, rgb3, rgb2, pixels, width, height);
			else if (y2 <= y1 && y1 <= y3)
				drawGouraudTriangle(x2, y2, x1, y1, x3, y3, rgb2, rgb1, rgb3, pixels, width, height);
			else if (y2 <= y3 && y3 <= y1)
				drawGouraudTriangle(x2, y2, x3, y3, x1, y1, rgb2, rgb3, rgb1, pixels, width, height);
			else if (y3 <= y1 && y1 <= y2)
				drawGouraudTriangle(x3, y3, x1, y1, x2, y2, rgb3, rgb1, rgb2, pixels, width, height);
			else if (y3 <= y2 && y2 <= y1)
				drawGouraudTriangle(x3, y3, x2, y2, x1, y1, rgb3, rgb2, rgb1, pixels, width, height);

			return;
		}

		for (int y = y1; y < y2; y++) {
			int a = ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			int rgba = interpolateRGB(rgb1, rgb2, (y - y1) / (double) (y2 - y1));
			int rgbb = interpolateRGB(rgb1, rgb3, (y - y1) / (double) (y3 - y1));

			for (int x = Math.min(b, a); x < Math.max(b, a); x++)
				if (x > 0 && x < width && y < height && y > 0) {
					int rgb = b > a ? interpolateRGB(rgba, rgbb, (x - a) / (double) (b - a)) : interpolateRGB(rgbb,
							rgba, (x - b) / (double) (a - b));
					fillPixel(x, y, rgb, pixels, width, height);
				}
		}
		for (int y = y2; y < y3; y++) {
			int a = ((y - y2) * (x3 - x2)) / (y3 - y2) + x2;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			int rgba = interpolateRGB(rgb2, rgb3, (y - y2) / (double) (y3 - y2));
			int rgbb = interpolateRGB(rgb1, rgb3, (y - y1) / (double) (y3 - y1));

			for (int x = Math.min(b, a); x < Math.max(b, a); x++)
				if (x > 0 && x < width && y < height && y > 0) {

					int rgb = b > a ? interpolateRGB(rgba, rgbb, (x - a) / (double) (b - a)) : interpolateRGB(rgbb,
							rgba, (x - b) / (double) (a - b));
					fillPixel(x, y, rgb, pixels, width, height);
				}
		}
	}

	public void drawTexturedTriangle(int x1, int y1, int x2, int y2, int x3, int y3, double u1, double v1, double u2,
			double v2, double u3, double v3, BufferedImage img) {
		// y1<y2<y3 (Sorted Points)
		if (!(y1 <= y2 && y2 <= y3)) {
			if (y1 <= y3 && y3 <= y2)
				drawTexturedTriangle(x1, y1, x3, y3, x2, y2, u1, v1, u3, v3, u2, v2, img);
			else if (y2 <= y1 && y1 <= y3)
				drawTexturedTriangle(x2, y2, x1, y1, x3, y3, u2, v2, u1, v1, u3, v3, img);
			else if (y2 <= y3 && y3 <= y1)
				drawTexturedTriangle(x2, y2, x3, y3, x1, y1, u2, v2, u3, v3, u1, v1, img);
			else if (y3 <= y1 && y1 <= y2)
				drawTexturedTriangle(x3, y3, x1, y1, x2, y2, u3, v3, u1, v1, u2, v2, img);
			else if (y3 <= y2 && y2 <= y1)
				drawTexturedTriangle(x3, y3, x2, y2, x1, y1, u3, v3, u2, v2, u1, v1, img);

			return;
		}

		for (int y = y1; y < y2; y++) {
			int a = ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			double ua = (u2 - u1) * (y - y1) / (double) (y2 - y1) + u1;
			double va = (v2 - v1) * (y - y1) / (double) (y2 - y1) + v1;
			double ub = (u3 - u1) * (y - y1) / (double) (y3 - y1) + u1;
			double vb = (v3 - v1) * (y - y1) / (double) (y3 - y1) + v1;

			for (int x = Math.min(b, a); x < Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					double u = b > a ? (ub - ua) * (x - a) / (double) (b - a) + ua : (ua - ub) * (x - b)
							/ (double) (a - b) + ub;
					double v = b > a ? (vb - va) * (x - a) / (double) (b - a) + va : (va - vb) * (x - b)
							/ (double) (a - b) + vb;
					fillPixel(x, y, img.getRGB((int) (img.getWidth() * u), (int) (img.getHeight() * v)));
				}
		}
		for (int y = y2; y < y3; y++) {
			int a = ((y - y2) * (x3 - x2)) / (y3 - y2) + x2;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			double ua = (u3 - u2) * (y - y2) / (double) (y3 - y2) + u2;
			double va = (v3 - v2) * (y - y2) / (double) (y3 - y2) + v2;
			double ub = (u3 - u1) * (y - y1) / (double) (y3 - y1) + u1;
			double vb = (v3 - v1) * (y - y1) / (double) (y3 - y1) + v1;

			for (int x = Math.min(b, a); x <= Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					double u = b > a ? (ub - ua) * (x - a) / (double) (b - a) + ua : (ua - ub) * (x - b)
							/ (double) (a - b) + ub;
					double v = b > a ? (vb - va) * (x - a) / (double) (b - a) + va : (va - vb) * (x - b)
							/ (double) (a - b) + vb;

					fillPixel(x, y, img.getRGB((int) (img.getWidth() * u), (int) (img.getHeight() * v)));
				}
		}
	}

	public static int interpolateRGB(int rgb1, int rgb2, double percent) {
		// int a1 = (rgb1 >> 24) & 0xff;
		int r1 = (rgb1 >> 16) & 0xff;
		int g1 = (rgb1 >> 8) & 0xff;
		int b1 = (rgb1 >> 0) & 0xff;

		// int a2 = (rgb2 >> 24) & 0xff;
		int r2 = (rgb2 >> 16) & 0xff;
		int g2 = (rgb2 >> 8) & 0xff;
		int b2 = (rgb2 >> 0) & 0xff;

		return (/* ((int)(a1+(a2-a1)*percent)<<16) */(255 << 24) | ((int) (r1 + (r2 - r1) * percent) << 16)
				| ((int) (g1 + (g2 - g1) * percent) << 8) | (int) (b1 + (b2 - b1) * percent));
	}

	public void drawLine(int x1, int y1, int x2, int y2, int rgb) {
		int dy = y2 - y1;
		int dx = x2 - x1;
		float t = 0.5F; // offset for rounding

		fillPixel(x1, y1, rgb);

		if (Math.abs(dx) > Math.abs(dy)) // slope < 1
		{
			float m = (float) dy / (float) dx; // compute slope
			t += y1;
			dx = (dx < 0) ? -1 : 1;
			m *= dx;
			while (x1 != x2) {
				x1 += dx; // step to next x value
				t += m; // add slope to y value

				fillPixel(x1, (int) t, rgb);
			}
		} else// slope >= 1
		{
			float m = (float) dx / (float) dy; // compute slope
			t += x1;
			dy = (dy < 0) ? -1 : 1;
			m *= dy;
			while (y1 != y2) {
				y1 += dy; // step to next y value
				t += m; // add slope to x value

				fillPixel((int) t, y1, rgb);
			}
		}
	}

	public void drawLine(int x1, int y1, int x2, int y2) {
		drawLine(x1, y1, x2, y2, (200 << 24) | (255 << 16) | (255 << 8) | 255);
	}

	public void fillPixel(int x, int y, int rgb) {
		fillPixel(x, y, rgb, pixels, WIDTH, HEIGHT);
	}

	public static void fillPixel(int x, int y, int rgb, int pixels[], int width, int height) {
		if (x < width && x >= 0 && y >= 0 && y < height)
			pixels[x + y * width] = interpolateRGB(pixels[x + y * width], rgb, (rgb >> 24 & 0xff) / 255.0D);
	}

	public void draw(GamePanel p, ParticleCloud3D s) {
		for (int i = 0; i < s.x.length; i++) {
			Vector2D pos = p.screenPos(s.z[i], s.y[i], s.z[i]);
			fillPixel((int) pos.x, (int) pos.y, s.rgb[i]);
		}
	}

	public void draw(ParticleCloud3D s, int px, int py, int t) {
		for (int i = 0; i < s.x.length; i++) {
			for (int x = (int) (s.x[i] - t); x < s.x[i] + t; x++)
				for (int y = (int) (s.y[i] - t); y < s.y[i] + t; y++)
					fillPixel(x - px, y - py, s.rgb[i]);
		}
	}
}
