package lowlevel;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TransperencyFilter extends AbstractBufferedImageOp {
	@Override
	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		int width = src.getWidth();
		int height = src.getHeight();

		if (dst == null)
			dst = createCompatibleDestImage(src, null);

		int[] inPixels = new int[width * height];
		int[] outPixels = new int[width * height];
		getRGB(src, 0, 0, width, height, inPixels);

		for (int i = 0; i < inPixels.length; i++) {
			int min = Methods.minI(inPixels[i] >> 16 & 0xff, inPixels[i] >> 8 & 0xff, inPixels[i] & 0xff);
			outPixels[i] = ((inPixels[i] & 0xff) - min) | ((((inPixels[i] >> 8) & 0xff) - min) << 8)
					| ((((inPixels[i] >> 16) & 0xff) - min) << 16)
					| (Math.min(255 - min, (inPixels[i] >> 24 & 0xff)) << 24);
		}

		setRGB(dst, 0, 0, width, height, outPixels);
		return dst;
	}

	public BufferedImage createCompatibleDestImage(BufferedImage src, ColorModel dstCM) {
		if (dstCM == null)
			dstCM = src.getColorModel();
		return new BufferedImage(dstCM, dstCM.createCompatibleWritableRaster(src.getWidth(), src.getHeight()), dstCM
				.isAlphaPremultiplied(), null);
	}

	public static void main(String[] args) throws IOException {
		while (true) {
			JFileChooser jfc = new JFileChooser();
			jfc.setCurrentDirectory(new File("Resources/Images"));

			if (jfc.showDialog(null, "Convert") != JFileChooser.APPROVE_OPTION)
				System.exit(0);

			JPanel panel = new JPanel(new BorderLayout());
			JLabel l1 = new JLabel();
			panel.add(l1, BorderLayout.WEST);
			JLabel l2 = new JLabel();
			panel.add(l2, BorderLayout.EAST);

			BufferedImage image = ImageIO.read(jfc.getSelectedFile());
			TransperencyFilter filter = new TransperencyFilter();

			BufferedImage diff = filter.filter(image, new BufferedImage(image.getWidth(), image.getHeight(),
					BufferedImage.TYPE_INT_ARGB));

			l1.setIcon(new ImageIcon(image.getScaledInstance(500, 500, Image.SCALE_SMOOTH)));
			l2.setIcon(new ImageIcon(diff.getScaledInstance(500, 500, Image.SCALE_SMOOTH)));

			if (JOptionPane.showConfirmDialog(null, panel) == 0)
				ImageIO.write(diff, "png", new File(jfc.getCurrentDirectory(), JOptionPane.showInputDialog("Name")
						+ ".png"));
		}
	}
}
