package creation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import lowlevel.Methods;
import lowlevel.PixelDrawer;
import math.Matrix;
import math.Vector3D;
import framework.BodyPart;

public class CreateModelPanel extends JPanel implements Runnable, MouseListener, MouseMotionListener,
		MouseWheelListener, ActionListener, KeyListener, ListSelectionListener {
	private static final long serialVersionUID = -4477994293019181948L;
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	JPanel bottom = new JPanel();
	JPanel top = new JPanel(new FlowLayout(FlowLayout.CENTER));
	JButton newBodyPart = new JButton("New Body Part");
	JButton delete = new JButton("Delete Current Body Part");
	JComboBox<String> shape = new JComboBox<String>(new String[] { "Square", "Circle", "Line", "Triangle" });
	JComboBox<String> mode = new JComboBox<String>(new String[] { "New Shape", "Shape Properties",
			"Shape Repositoining" });
	Set<Integer> down = new HashSet<Integer>();

	JList<BodyPart> list = new JList<BodyPart>();
	JScrollPane listPane = new JScrollPane(list);

	double alpha = 0, beta = 0, gamma = 0, aTo, bTo, gTo;
	Vector3D up = new Vector3D(0, 0, 1), right = new Vector3D(1, 0, 0);

	JToggleButton focusComponent = new JToggleButton("Movable");

	Matrix rot, projX = Matrix.create3DProjectionMatrix(right), projY = Matrix.create3DProjectionMatrix(up);

	ArrayList<BodyPart> parts = new ArrayList<BodyPart>();
	PixelDrawer graphics = new PixelDrawer(SCREEN.width - 150, SCREEN.height - 35, this);
	BodyPart current = null;

	public CreateModelPanel() {
		setLayout(new BorderLayout());

		listenTo(this);

		add(top, BorderLayout.NORTH);
		add(bottom, BorderLayout.SOUTH);
		add(listPane, BorderLayout.WEST);

		BodyPart bp = new BodyPart();
		bp.name = "DEFAULT";

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(0).add(new Vector3D(-50, -50, -50));
		bp.points.get(0).add(new Vector3D(-50, -50, 50));
		bp.colors.add(Color.BLACK);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(1).add(new Vector3D(-50, -50, -50));
		bp.points.get(1).add(new Vector3D(50, -50, -50));
		bp.colors.add(Color.RED);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(2).add(new Vector3D(-50, -50, -50));
		bp.points.get(2).add(new Vector3D(-50, 50, -50));
		bp.colors.add(Color.GREEN);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(3).add(new Vector3D(50, 50, 50));
		bp.points.get(3).add(new Vector3D(50, 50, -50));
		bp.colors.add(Color.BLACK);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(4).add(new Vector3D(50, 50, 50));
		bp.points.get(4).add(new Vector3D(50, -50, 50));
		bp.colors.add(Color.GREEN);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(5).add(new Vector3D(50, 50, 50));
		bp.points.get(5).add(new Vector3D(-50, 50, 50));
		bp.colors.add(Color.RED);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(6).add(new Vector3D(-50, 50, -50));
		bp.points.get(6).add(new Vector3D(50, 50, -50));
		bp.colors.add(Color.RED);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(7).add(new Vector3D(50, -50, 50));
		bp.points.get(7).add(new Vector3D(-50, -50, 50));
		bp.colors.add(Color.RED);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(8).add(new Vector3D(-50, 50, -50));
		bp.points.get(8).add(new Vector3D(-50, 50, 50));
		bp.colors.add(Color.BLACK);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(9).add(new Vector3D(50, -50, 50));
		bp.points.get(9).add(new Vector3D(50, -50, -50));
		bp.colors.add(Color.BLACK);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(10).add(new Vector3D(50, 50, -50));
		bp.points.get(10).add(new Vector3D(50, -50, -50));
		bp.colors.add(Color.GREEN);
		bp.types.add(2);
		bp.widths.add(5);

		bp.points.add(new ArrayList<Vector3D>());
		bp.points.get(11).add(new Vector3D(-50, -50, 50));
		bp.points.get(11).add(new Vector3D(-50, 50, 50));
		bp.colors.add(Color.GREEN);
		bp.types.add(2);
		bp.widths.add(5);

		current = bp;
		parts.add(bp);

		listPane.setPreferredSize(new Dimension(150, 200));

		focusComponent.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// focusComponent.setSelected(true);
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				focusComponent.setSelected(false);
			}
		});

		bottom.setLayout(new FlowLayout(FlowLayout.CENTER));
		bottom.add(focusComponent);
		bottom.add(newBodyPart);
		bottom.add(delete);
		top.add(mode);
		top.add(shape);

		bottom.setBackground(Color.BLACK);
		top.setBackground(Color.GRAY);
		list.setBackground(Color.GRAY);
		list.setForeground(Color.WHITE);

		listenTo(newBodyPart);
		listenTo(list);
		listenTo(delete);
		listenTo(focusComponent);
		listenTo(mode);
		list.addListSelectionListener(this);

		list.setListData(Methods.createArray(parts, new BodyPart[parts.size()]));
		new Thread(this).start();
	}

	public void listenTo(JComponent c) {
		c.addMouseListener(this);
		c.addMouseMotionListener(this);
		c.addMouseWheelListener(this);
		c.addKeyListener(this);
		if (c instanceof JButton)
			((JButton) c).addActionListener(this);
	}

	public void run() {

		while (true) {
			try {
				Thread.sleep(50);
			} catch (Exception e) {
			}

			// if (down.contains(KeyEvent.VK_UP))
			// beta += Math.PI / 36;
			// if (down.contains(KeyEvent.VK_DOWN))
			// beta -= Math.PI / 36;
			// if (down.contains(KeyEvent.VK_LEFT))
			// alpha += Math.PI / 36;
			// if (down.contains(KeyEvent.VK_RIGHT))
			// alpha -= Math.PI / 36;

			if (Math.abs(aTo - alpha) > .1)
				alpha += (aTo - alpha) / 4 + Math.signum(aTo - alpha) / 50;
			else
				alpha = aTo;

			if (Math.abs(bTo - beta) > .1)
				beta += (bTo - beta) / 4 + Math.signum(bTo - beta) / 50;
			else
				beta = bTo;

			if (Math.abs(gTo - gamma) > .1)
				gamma += (gTo - gamma) / 4 + Math.signum(gTo - gamma) / 50;
			else
				gamma = gTo;

			up = Vector3D.findUp(alpha, beta, gamma);
			right = Vector3D.findRight(alpha, beta, gamma);

			projX = Matrix.create3DProjectionMatrix(right);
			projY = Matrix.create3DProjectionMatrix(up);
			rot = Matrix.create3DRotationMatrix(-alpha, -beta, -gamma);

			graphics.clear(focusComponent.isSelected() ? Color.WHITE.getRGB() : Color.LIGHT_GRAY.getRGB());

			for (BodyPart b : parts)
				draw(b);

			graphics.update();
			repaint();
		}
	}

	public void draw(BodyPart b) {
		for (int i = 0; i < b.types.size(); i++) {
			int rgb = Methods.colorMeld(b.colors.get(i), focusComponent.isSelected() ? Color.WHITE : Color.LIGHT_GRAY,
					b == current ? 0 : .7).getRGB();
			if (down.contains(KeyEvent.VK_SPACE)) {
				switch (b.types.get(i)) {
				case 0:
					break;
				case 1:
					break;
				case 2:
					break;
				case 3:
					break;
				}
			} else {
				switch (b.types.get(i)) {
				case 0:
					graphics.fillRect((int) projX.eval(b.points.get(i).get(0)).getDirectionalMagnitude(right) - 75
							+ SCREEN.width / 2 - b.widths.get(i) / 2, (int) projY.eval(b.points.get(i).get(0))
							.getDirectionalMagnitude(up) - 17 + SCREEN.height / 2 - b.widths.get(i) / 2,
							b.widths.get(i), b.widths.get(i), rgb);
					break;
				case 1:
					graphics.fillOval((int) projX.eval(b.points.get(i).get(0)).getDirectionalMagnitude(right) - 75
							+ SCREEN.width / 2 - b.widths.get(i) / 2, (int) projY.eval(b.points.get(i).get(0))
							.getDirectionalMagnitude(up) - 17 + SCREEN.height / 2 - b.widths.get(i) / 2,
							b.widths.get(i), b.widths.get(i), rgb);
					break;
				case 2:
					graphics.drawLine((int) projX.eval(b.points.get(i).get(0)).getDirectionalMagnitude(right) - 75
							+ SCREEN.width / 2, (int) projY.eval(b.points.get(i).get(0)).getDirectionalMagnitude(up)
							- 17 + SCREEN.height / 2,
							(int) projX.eval(b.points.get(i).get(1)).getDirectionalMagnitude(right) - 75 + SCREEN.width
									/ 2, (int) projY.eval(b.points.get(i).get(1)).getDirectionalMagnitude(up)
									+ SCREEN.height / 2 - 17, rgb);
					break;
				case 3:
					graphics.drawTriangle((int) projX.eval(b.points.get(i).get(0)).getDirectionalMagnitude(right) - 75
							+ SCREEN.width / 2, (int) projY.eval(b.points.get(i).get(0)).getDirectionalMagnitude(up)
							+ SCREEN.height / 2 - 17,
							(int) projX.eval(b.points.get(i).get(1)).getDirectionalMagnitude(right) - 75 + SCREEN.width
									/ 2, (int) projY.eval(b.points.get(i).get(1)).getDirectionalMagnitude(up)
									+ SCREEN.height / 2 - 17, (int) projX.eval(b.points.get(i).get(2))
									.getDirectionalMagnitude(right) - 75 + SCREEN.width / 2,
							(int) projY.eval(b.points.get(i).get(0)).getDirectionalMagnitude(up) + SCREEN.height / 2
									- 17, rgb);
					break;
				}
			}
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(graphics.getImage(), 150, 35, this);
		g.fillOval(
				list.getWidth() / 2 + SCREEN.width / 2 - 15
						- (int) ((SCREEN.width / 2 - list.getWidth()) * Math.sin(gamma)), bottom.getHeight() / 2 - 15
						+ SCREEN.height / 2 - (int) ((SCREEN.height / 2 - bottom.getHeight()) * Math.cos(gamma)), 30,
				30);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// focusComponent.requestFocus();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		switch (mode.getSelectedIndex()) {
		case 0:
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent evt) {
		if (list.getSelectedIndex() >= 0)
			current = parts.get(list.getSelectedIndex());
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent evt) {
		down.add(evt.getKeyCode());

		if (focusComponent.isSelected()) {
			if (evt.getKeyCode() == KeyEvent.VK_A)
				aTo += Math.PI / 6;
			else if (evt.getKeyCode() == KeyEvent.VK_D)
				aTo -= Math.PI / 6;
			else if (evt.getKeyCode() == KeyEvent.VK_W)
				bTo += Math.PI / 6;
			else if (evt.getKeyCode() == KeyEvent.VK_S)
				bTo -= Math.PI / 6;
			else if (evt.getKeyCode() == KeyEvent.VK_Q)
				gTo += Math.PI / 6;
			else if (evt.getKeyCode() == KeyEvent.VK_E)
				gTo -= Math.PI / 6;
		}

	}

	public void keyReleased(KeyEvent evt) {
		down.remove(evt.getKeyCode());

		// focusComponent.requestFocus();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent evt) {
		// gTo += evt.getWheelRotation() / 100D;

	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == newBodyPart) {
			current = new BodyPart();
			current.name = JOptionPane.showInputDialog("Name:");
			current.position = new Vector3D(0, 0, 0);
			current.points.add(new ArrayList<Vector3D>());
			parts.add(current);
			list.setListData(Methods.createArray(parts, new BodyPart[parts.size()]));

		} else if (evt.getSource() == delete) {
			if (parts.indexOf(current) > 0)
				current = parts.get(parts.indexOf(current) - 1);
			else if (parts.size() > 1)
				current = parts.get(parts.indexOf(current) + 1);
			else
				current = null;
			parts.remove(current);
			list.setListData(Methods.createArray(parts, new BodyPart[parts.size()]));
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("CREATOR");

		CreateModelPanel p = new CreateModelPanel();
		frame.add(p, BorderLayout.CENTER);

		frame.setSize(SCREEN);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.setVisible(true);
		p.focusComponent.requestFocus();
	}

}
