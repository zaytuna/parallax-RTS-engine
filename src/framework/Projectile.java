package framework;

import java.awt.Color;

import lowlevel.ParticleCloud3D;
import math.Vector3D;
import gui.GameWindow;

public class Projectile extends Vector3D {
	// Mice: Bullets, fire, rockets, shells, laser cannons
	// Artists: Pencils, light beams
	// Earth: element-balls, earthquakes

	public Color c;
	public Vector3D target; // could be a unit or simply a position;
	public boolean homing, explosive;
	public double vx, vy, vz, calibur, damage;

	public Projectile(Unit u, Vector3D target) {
		super(u.x, u.y, u.z);

		this.damage = u.attack;
		this.calibur = u.maxHealth;
		this.vx = (target.x - u.x) / u.bulletTime;
		this.vy = (target.y - u.y) / u.bulletTime;
		this.vz = (target.z + 10 - u.z) / u.bulletTime;
		this.c = new Color(u.getRGB());

		this.homing = u.homingRounds;
		this.explosive = u.explosiveRounds;
		
		this.target = target;
	}

	public void update() {
		vz -= .001;

		if (homing) {
			vx += (target.x - x) / 30;
			vy += (target.y - y) / 30;
		}

		x += vx;
		y += vy;
		z += vz;

		if (Math.abs(x - target.x) < 2 && Math.abs(y - target.y) < 10 && Math.abs(z - target.z) < 10) {
			if (explosive)
				GameWindow.self.display.world.addEffect(new ParticleCloud3D(500, c, 20, target.x, target.y, target.z));
		}
	}
}
