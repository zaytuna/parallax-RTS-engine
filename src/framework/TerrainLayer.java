package framework;

public class TerrainLayer<T> {
	private T[][] values;
	public Terrain model;

	@SuppressWarnings("unchecked")
	public TerrainLayer(Terrain t) {
		model = t;
		values = (T[][]) new Object[t.getWidth()][t.getHeight()];
	}

	@SuppressWarnings("unchecked")
	public synchronized void resize(Terrain t, T defaultT) {
		model = t;
		T[][] test = (T[][]) new Object[model.getWidth()][model.getHeight()];

		for (int i = 0; i < test.length; i++)
			for (int j = 0; j < test[i].length; j++)
				test[i][j] = (i >= values.length || j >= values[i].length) ? defaultT : values[i][j];

		values = test;
	}

	public synchronized void put(T item, int x, int y) {
		values[x][y] = item;
	}

	public synchronized T get(int x, int y) {
		return values[x][y];
	}

	public synchronized void fill(T item) {
		for (int i = 0; i < values.length; i++)
			for (int j = 0; j < values[i].length; j++)
				put(item, i, j);
	}
}
