package framework;

import gui.GamePanel;
import lowlevel.PixelDrawer;
import math.Vector3D;

public interface Effect {
	public void draw(PixelDrawer p, GamePanel gp);

	public void update();

	public Vector3D getPosition();

	public boolean isDead();
}
