package framework;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import lowlevel.GaussianFilter;
import lowlevel.Methods;
import math.Vector3D;
import gui.RuntimeOptions;

public class Terrain implements Iterable<Terrain.MapVertex> {
	public static Image[] textures = new Image[7];// Water,Sand,Grass,Red Rock,
	// Dirt,Rock,Snow

	public static int GRAIN = 10;

	public static final int NORTH = 0;
	public static final int NORTH_WEST = 1;
	public static final int WEST = 2;
	public static final int SOUTH_WEST = 3;
	public static final int SOUTH = 4;
	public static final int SOUTH_EAST = 5;
	public static final int EAST = 6;
	public static final int NORTH_EAST = 7;

	MapVertex[][] vertices;
	Point[][] resources;

	public double percentDone;

	public Vector3D sunDir = new Vector3D(1, 0, .6);

	private BufferedImage mirror;

	Terrain(int x, int y) {
		vertices = new MapVertex[x][y];
		for (int i = 0; i < x; i++)
			for (int j = 0; j < y; j++)
				vertices[i][j] = new MapVertex(i, j, 0);
	}

	public Terrain clone() {
		Terrain t = new Terrain(getWidth(), getHeight());
		t.sunDir = sunDir.clone();
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++) {
				t.vertices[i][j].color = Methods.clone(vertices[i][j].color);
				t.vertices[i][j].shadowed = vertices[i][j].shadowed;
				t.vertices[i][j].type = vertices[i][j].type;
				t.vertices[i][j].x = vertices[i][j].x;
				t.vertices[i][j].y = vertices[i][j].y;
				t.vertices[i][j].z = vertices[i][j].z;
				t.vertices[i][j].normal = vertices[i][j].normal.clone();
			}

		return t;
	}

	public void setColors(Terrain t, int minX, int minY, int maxX, int maxY) {
		sunDir = t.sunDir.clone();
		for (int i = minX; i < maxX; i++)
			for (int j = minY; j < maxY; j++) {
				vertices[i][j].color = Methods.clone(t.vertices[i][j].color);
				vertices[i][j].shadowed = t.vertices[i][j].shadowed;
			}

	}

	public void smoothColors(int radius, double percent) {
		smoothColors(radius, 0, 0, vertices.length, vertices[0].length, percent);
	}

	public void smoothColors(int radius, int minX, int minY, int maxX, int maxY, double percent) {
		Color[][] map = new Color[vertices.length][vertices[0].length];
		for (int i = minX; i < maxX; i++) {
			for (int j = minY; j < maxY; j++) {

				double r = 0, g = 0, b = 0;
				int count = 0;
				for (int q = -radius; q <= radius; q++)
					for (int k = -radius; k <= radius; k++)
						try {
							r += vertices[i + q][j + k].color.getRed();
							g += vertices[i + q][j + k].color.getGreen();
							b += vertices[i + q][j + k].color.getBlue();
							count++;
						} catch (IndexOutOfBoundsException ioobe) {
						}

				r /= count;
				g /= count;
				b /= count;
				map[i][j] = new Color((int) r, (int) g, (int) b);
			}
		}

		for (int i = minX; i < maxX; i++)
			for (int j = minY; j < maxY; j++)
				vertices[i][j].color = new Color(map[i][j].getRed(), map[i][j].getGreen(), map[i][j].getBlue());
	}

	public Color getColor(int x, int y) {
		if (vertices[x][y].color == null)
			vertices[x][y].color = Color.MAGENTA;
		return vertices[x][y].color;
	}

	public Color getRawColor(int x, int y, int type, int seed) {
		ArrayList<Color> clrs = new ArrayList<Color>();

		int k = RuntimeOptions.terrainSmooth;

		for (int i = x - k; i <= x + k; i++)
			for (int j = y - k; j <= y + k; j++)
				try {
					clrs.add(getTypeColor(vertices[i][j].type, seed));
				} catch (IndexOutOfBoundsException e) {
				}

		return Methods.colorAvg(clrs);
	}

	public static Color getTypeColor(int type, int seed) {
		// "Water","Sand","Grass","Red Rock","Dirt","Rock","Tundra"
		Color c = null;
		switch (type) {
		case 0:
			c = new Color(11, 38, 217);
			break;
		case 1:
			c = new Color(216, 193, 152);
			break;
		case 2:
			c = new Color(11, 139, 0);
			break;
		case 3:
			c = new Color(217, 126, 74);
			break;
		case 4:
			c = new Color(130, 85, 52);
			break;
		case 5:
			c = new Color(120, 129, 123);
			break;
		case 6:
			c = new Color(230, 255, 255);
			break;
		case 7:
			// return new Color
		}
		return new Color((int) Math.min(Math.max(c.getRed() + Methods.pseudoRandom(seed) * RuntimeOptions.options.radius,
				0), 255), (int) Math.min(Math.max(c.getGreen() + Methods.pseudoRandom(seed)
				* RuntimeOptions.options.radius, 0), 255), (int) Math.min(Math.max(c.getBlue()
				+ Methods.pseudoRandom(seed) * RuntimeOptions.options.radius, 0), 255));
	}

	public void smooth(int r) {
		double[][] map = new double[vertices.length][vertices[0].length];
		for (int i = 0; i < vertices.length; i++) {
			for (int j = 0; j < vertices[i].length; j++) {
				double avg = 0;
				int count = 0;
				for (int q = -r; q < r + 1; q++)
					for (int k = -r; k < r + 1; k++)
						try {
							avg += vertices[i + q][j + k].z;
							count++;
						} catch (IndexOutOfBoundsException ioobe) {
						}

				avg = avg / (double) count;
				map[i][j] = avg;
			}
		}

		for (int i = 0; i < vertices.length; i++)
			for (int j = 0; j < vertices[i].length; j++)
				vertices[i][j].z = map[i][j];
	}

	public void smoothTiling(int r) {
		int[][] map = new int[vertices.length][vertices[0].length];
		for (int i = 0; i < vertices.length; i++) {
			for (int j = 0; j < vertices[i].length; j++) {
				if (vertices[i][j].type != 0) {
					double avg = 0;
					int count = 0;
					for (int q = -r; q < r + 1; q++)
						for (int k = -r; k < r + 1; k++)
							if (i + q < vertices.length && i + q > 0 && j + k > 0 && j + k < vertices[i + q].length) {
								avg += vertices[i + q][j + k].type;
								count++;
							}

					avg = avg / (double) count;
					map[i][j] = vertices[i][j].type == 0 ? 0 : (int) Math.round(avg);
				}
			}
		}

		for (int i = 0; i < vertices.length; i++)
			for (int j = 0; j < vertices[i].length; j++)
				vertices[i][j].type = map[i][j];
	}

	public static Terrain fractal(double percentWater, double roughness, int lod) {

		int divisions = 1 << lod;
		Terrain t = new Terrain(divisions + 1, divisions + 1);
		t.fill(-1234);

		t.setHeightAt(0, 0, Math.random() * roughness - roughness / 2);
		t.setHeightAt(divisions, 0, Math.random() * roughness - roughness / 2);
		t.setHeightAt(0, divisions, Math.random() * roughness - roughness / 2);
		t.setHeightAt(divisions, divisions, Math.random() * roughness - roughness / 2);

		t.diamondSquare(0, 0, divisions, roughness, lod);
		t.tile(percentWater, 20);

		t.computeNormals();
		t.updateColorsAndLighting();
		return t;
	}

	public void setHeightAt(int x, int y, double h) {
		vertices[x][y].z = h;
	}

	public void fill(double d) {
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++)
				vertices[i][j].z = d;
	}

	private double random() {
		return 2 * Math.random() - 1;
	}

	public void setLinearHeightAt(int x, int y, double h, double slope) {
		double dh = h - getHeightAt(x, y);
		if (dh < 0)
			slope = -Math.abs(slope);
		int distanceFromBase = (int) (dh / slope);

		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				try {
					double distanceFromXY = Math.sqrt(Math.pow(i - x, 2) + Math.pow(j - y, 2));
					vertices[i][j].z += slope * Math.max(distanceFromBase - distanceFromXY, 0);
				} catch (IndexOutOfBoundsException ioobe) {
				}
			}
		}

	}

	public void setLogisticHeightAt(int x, int y, double h, double slope, int radius) {
		double dh = h - getHeightAt(x, y);
		for (int i = 0; i < vertices.length; i++) {
			for (int j = 0; j < vertices[i].length; j++) {
				double distanceFromXY = Math.sqrt(Math.pow(i - x, 2) + Math.pow(j - y, 2));
				vertices[i][j].z += (dh / (1 + Math.pow(Math.E, (distanceFromXY * slope - radius))));
			}
		}

	}

	public void setLinearHeightAt(Rectangle rect, double h, double slope) {// FIXME
		double dh = h - avgHeight(rect);
		slope = Math.signum(dh) * Math.abs(slope);
		int distanceFromBase = (int) (h / slope);

		for (int i = (rect.x - distanceFromBase); i < (rect.x + rect.width + distanceFromBase); i++) {
			for (int j = (rect.y - distanceFromBase); j < (rect.y + rect.height + distanceFromBase); j++) {
				try {
					Point distPoint = Methods.getClosestPointOn(rect, new Point(i, j));
					double distanceFromXY = Math.sqrt(Math.pow(distPoint.x - i, 2) + Math.pow(distPoint.y - j, 2));
					vertices[i][j].z += slope * Math.max(distanceFromBase - distanceFromXY, 0);
				} catch (IndexOutOfBoundsException ioobe) {
				}
			}
		}

	}

	public void setLogisticHeightAt(Rectangle rect, double h, double slope) {
		double dh = h - avgHeight(rect);
		for (int i = 0; i < vertices.length; i++) {
			for (int j = 0; j < vertices[i].length; j++) {
				Point distPoint = Methods.getClosestPointOn(rect, new Point(i, j));
				double distanceFromXY = Math.sqrt(Math.pow(i - distPoint.x, 2) + Math.pow(j - distPoint.y, 2));
				vertices[i][j].z += dh / (1 + Math.pow(Math.E, (distanceFromXY * slope - (2 * Math.E))));
			}
		}

	}

	public void tile(double percentWater, int size) {
		Point[] points = new Point[(int) (Math.random() * getWidth() * getHeight() / 6D)];
		int[] colors = new int[points.length];
		int[] radii = new int[points.length];

		// double max0 = maxHeight(), min0 = minHeight();
		for (int q = 0; q < points.length; q++) {
			points[q] = new Point((int) (Math.random() * getWidth()), (int) (Math.random() * getHeight()));
			// colors[q] = (int) (6 *Math.random()/Math.random()*
			// (vertices[points[q].x][points[q].y].z - min0) / (max0 -
			// min0)-2+3*Math.random());
			colors[q] = (int) (Math.random() * 6) + 1;
			radii[q] = (int) (Math.random() * size) + 5;
		}

		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++) {
				for (int l = 0; l < points.length; l++)
					if ((i - points[l].x) * (i - points[l].x) + (j - points[l].y) * (j - points[l].y) < radii[l]
							* radii[l]) {
						vertices[i][j].type = colors[l];
						break;
					} else
						vertices[i][j].type = (int) (Math.random() * 6) + 1;
			}

		smoothTiling(size * 3 / 4);
		double min = minHeight() + percentWater * (maxHeight() - minHeight());

		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++)
				if (getHeightAt(i, j) <= min) {
					setHeightAt(i, j, min);
					vertices[i][j].type = 0;
				}

	}

	public void levelify(int x) {
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++)
				vertices[i][j].z = x * vertices[i][j].type;
	}

	public void updateShadows() {
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++) {
				vertices[i][j].shadowed = shadowed(vertices[i][j], 2);
			}
	}

	public void updateShadow(int i, int j) {
		vertices[i][j].shadowed = shadowed(vertices[i][j], 2);
	}

	public void updateColorsAndLighting() {
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++) {
				vertices[i][j].color = Methods.shade(getRawColor(i, j, vertices[i][j].type, i * j + i * i + j * j + i
						- j), getPercent(vertices[i][j].normal.angleBetween(sunDir), vertices[i][j].shadowed));
			}
	}

	public void updateColorAndLight(int i, int j) {
		vertices[i][j].color = Methods.shade(getRawColor(i, j, vertices[i][j].type, i * j + i * i + j * j + i - j),
				getPercent(vertices[i][j].normal.angleBetween(sunDir), vertices[i][j].shadowed));
	}

	public static Color alterColor(Color c, int amount) {
		return new Color(Math.min(Math.max(c.getRed() - amount + (int) (2 * Math.random() * amount), 0), 255), Math
				.min(Math.max(c.getGreen() - amount + (int) (2 * Math.random() * amount), 0), 255), Math.min(Math.max(c
				.getBlue()
				- amount + (int) (2 * Math.random() * amount), 0), 255));
	}

	public static int getRGB(double z) {
		return new Color(Math.abs((int) (Methods.pseudoRandom((int) z) * 255)), Math.abs((int) (Methods
				.pseudoRandom((int) z - 1) * 255)), Math.abs((int) (Methods.pseudoRandom((int) z + 1) * 255))).getRGB();
	}

	public double getPercent(double angle, boolean shadowed) {
		if (!RuntimeOptions.options.shadows)
			shadowed = false;
		return (angle >= Math.PI / 2 || shadowed) ? RuntimeOptions.options.ambientLight : Math.cos(angle)
				* (1 - RuntimeOptions.options.ambientLight) + RuntimeOptions.options.ambientLight;
		// return Math.abs(Math.cos(angle)) * (1 - ambientLight) + ambientLight;
	}

	public void computeNormals() {
		for (int x = 0; x < getWidth(); x++)
			for (int y = 0; y < getHeight(); y++) {
				if (vertices[x][y].normal == null)
					vertices[x][y].normal = new Vector3D(0, 0, 0);
				else
					vertices[x][y].normal.set(0, 0, 0);

				// System.out.print("\n\nSUM: {");

				for (int i = 0; i < 8; i++) {
					try {
						Vector3D n = Plane.getNormalFromPoints(
								vertices[(int) vertices[x][y].x + getDX(i)][(int) vertices[x][y].y + getDY(i)],
								vertices[(int) vertices[x][y].x + getDX(i + 3)][(int) vertices[x][y].y + getDY(i + 3)],
								vertices[(int) vertices[x][y].x][(int) vertices[x][y].y]);

						if (n.z < 0)
							n.negate();

						n.unitize();
						// System.out.print(n);

						vertices[x][y].normal.add(n);
					} catch (IndexOutOfBoundsException e) {
					}
				}
				vertices[x][y].normal.unitize();
				// if(v.normal.y > 0)
				// System.out.println(v.normal);

			}
	}

	public Vector3D getNormal(int i, int j) {
		return vertices[i][j].normal;
	}

	public double avgHeight(Rectangle r) {
		double avg = 0;
		for (int i = r.x; i < r.width + r.x; i++)
			for (int j = r.y; j < r.height + r.y; j++)
				avg += getHeightAt(i, j);

		return avg / (r.width * r.height);
	}

	public void diamondSquare(int x, int y, int side, double roughness, int lodLeft) {
		if (lodLeft <= 0)
			return;

		if (getHeightAt(x + side / 2, y) == -1234)
			setHeightAt(x + side / 2, y, (vertices[x][y].z + vertices[x + side][y].z) / 2 + random() * roughness);
		if (getHeightAt(x + side / 2, y + side) == -1234)
			setHeightAt(x + side / 2, y + side, (vertices[x][y + side].z + vertices[x + side][y + side].z) / 2
					+ random() * roughness);
		if (getHeightAt(x, y + side / 2) == -1234)
			setHeightAt(x, y + side / 2, (vertices[x][y].z + vertices[x][y + side].z) / 2 + random() * roughness);
		if (getHeightAt(x + side, y + side / 2) == -1234)
			setHeightAt(x + side, y + side / 2, (vertices[x + side][y].z + vertices[x + side][y + side].z) / 2
					+ random() * roughness);

		setHeightAt(x + side / 2, y + side / 2, (vertices[x][y].z + vertices[x + side][y].z
				+ vertices[x + side][y + side].z + vertices[x][y + side].z)
				/ 4 + random() * roughness);

		diamondSquare(x, y, side / 2, roughness / 2, lodLeft - 1);
		diamondSquare(x, y + side / 2, side / 2, roughness / 2, lodLeft - 1);
		diamondSquare(x + side / 2, y, side / 2, roughness / 2, lodLeft - 1);
		diamondSquare(x + side / 2, y + side / 2, side / 2, roughness / 2, lodLeft - 1);
	}

	public double maxHeight() {
		double max = getHeightAt(0, 0);

		for (int i = 0; i < vertices.length; i++)
			for (int j = 0; j < vertices[i].length; j++)
				if (getHeightAt(i, j) > max)
					max = getHeightAt(i, j);

		return max;
	}

	public double minHeight() {
		double min = getHeightAt(0, 0);

		for (int i = 0; i < vertices.length; i++)
			for (int j = 0; j < vertices[i].length; j++)
				if (getHeightAt(i, j) < min)
					min = getHeightAt(i, j);

		return min;
	}

	public double avgHeight() {
		double total = 0;

		for (int i = 0; i < vertices.length; i++)
			for (int j = 0; j < vertices[i].length; j++)
				total += vertices[i][j].z;

		return total / (getWidth() * getHeight());
	}

	public static class MapVertex extends Vector3D {
		public int type;
		public Color color;
		public boolean shadowed;
		Vector3D normal = new Vector3D(0, 0, 1);

		public MapVertex(double a, double b, double c) {
			super(a, b, c);
		}
	}

	@Override
	public Iterator<MapVertex> iterator() {
		return new VIterator();
	}

	public void scale(double d) {
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++)
				setHeightAt(i, j, getHeightAt(i, j) * d);

	}

	public void multiply(Terrain t) {
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++) {
				setHeightAt(i, j, getHeightAt(i, j) * t.getHeightAt(i, j) / (maxHeight() * t.maxHeight()));
			}
	}

	public boolean shadowed(Vector3D p, int step) {
		if (!RuntimeOptions.options.shadows)
			return false;

		double dx = sunDir.x * step;
		double dy = sunDir.y * step;

		for (int i = 0; true; i++) {
			try {
				if (!(dx * i + p.x < getWidth() && dx * i + p.x > 0 && dy * i + p.y < getHeight() && dy * i + p.y > 0))
					break;

				if (getHeightAt((int) (dx * i + p.x), (int) (dy * i + p.y)) > p.z + sunDir.z * i * step
						&& getHeightAt((int) (dx * i + p.x) + 1, (int) (dy * i + p.y)) > p.z + sunDir.z * i * step
						&& getHeightAt((int) (dx * i + p.x), (int) (dy * i + p.y) + 1) > p.z + sunDir.z * i * step)
					return true;
			} catch (IndexOutOfBoundsException e) {
				break;
			}
		}

		return false;
	}

	public boolean isWalkable(Unit u, int x, int y, int dir) {
		return (u.moveType == 1 && vertices[x + getDX(dir)][y + getDY(dir)].type == 0)
				|| (u.moveType == 2)
				|| (u.moveType == 0 && vertices[x + getDX(dir)][y + getDY(dir)].type != 0 && u.versitility >= Math
						.abs(getSlope(x, y, dir)));
	}

	public static int getDX(int dir) {
		dir = dir % 8;
		return dir % 4 == 0 ? 0 : (dir / 4 == 0 ? -1 : 1);
	}

	public static int getDY(int dir) {
		dir = dir % 8;
		return dir % 4 == 2 ? 0 : (dir > 6 || dir < 2 ? -1 : 1);
	}

	public static double getUnitDistance(int dir) {
		return dir % 2 == 0 ? 1 : 1.414;
	}

	class VIterator implements Iterator<MapVertex> {
		MapVertex current = null;

		public boolean hasNext() {
			if (current == null)
				return true;
			return !(current.x == getWidth() - 1 && current.y == getHeight() - 1);
		}

		public MapVertex next() {
			if (current == null)
				current = getVertexAt(0, 0);
			else
				current = getVertexAt(((int) current.x + 1) % getWidth(), (int) current.y + ((int) current.x + 1)
						/ getWidth());

			return current;
		}

		public void remove() {
		}
	}

	public int getWidth() {
		return vertices.length;
	}

	public int getHeight() {
		return vertices[0].length;
	}

	public MapVertex getVertexAt(int i, int j) {
		return vertices[i][j];
	}

	public BufferedImage getImage() {
		if (mirror == null && vertices != null)
			createImage(190, 280);
		return mirror;
	}

	public void createImage(int x, int y) {
		if (vertices == null)
			return;
		mirror = new BufferedImage(x, y, BufferedImage.TYPE_INT_ARGB);
		Graphics g = mirror.getGraphics();
		double max = maxHeight(), min = minHeight();

		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++) {
				g.setColor(Methods.colorMeld(getColor(i, j), Color.WHITE, (getHeightAt(i, j) - min) / (max - min) * .5,
						255));
				g.fillRect((x * i) / getWidth(), (y * j) / getHeight(), 1 + x / getWidth(), 1 + y / getHeight());
			}
		g.dispose();

		// blur();
	}

	public double getHeightAt(int i, int j) {
		if (i >= 0 && j >= 0 && i < vertices.length && j < vertices[i].length)
			return vertices[i][j].z;
		return 0;
	}

	public void blur() {
		BufferedImage image = new BufferedImage(mirror.getWidth(), mirror.getHeight(), BufferedImage.TYPE_INT_ARGB);
		new GaussianFilter(5).filter(mirror, image);
		mirror = image;
	}

	public double getSlope(int x, int y, int dir) {
		try {
			return (getHeightAt(x, y) - getHeightAt(x + getDX(dir), y + getDY(dir))) / getUnitDistance(dir);
		} catch (IndexOutOfBoundsException e) {
			return 0;
		}
	}

	public double guess(int x1, int y1, int x2, int y2) {
		return Math.abs(x1 - x2) + Math.abs(y1 - y2);
	}

	public double getCost(Unit u, int x, int y, int dir) {
		// "Water","Sand","Grass","Red Rock","Dirt","Rock","Snow"
		double cost = 0;

		switch (vertices[x + getDX(dir)][y + getDY(dir)].type) {
		case 0:
			cost += u.moveType == 1 ? 10 / u.versitility : 40 / u.versitility;
			break;
		case 1:
			cost += 70;
			break;
		case 2:
			cost += 40;
			break;
		case 3:
			cost += 80;
			break;
		case 4:
			cost += 20;
			break;
		case 5:
			cost += 50;
			break;
		case 6:
			cost = 100;
		}

		if (u.moveType == 2)
			cost = 10;

		return (cost + (u.moveType == 2 ? 0 : 20 / u.versitility * Math.abs(getSlope(x, y, dir))))
				* getUnitDistance(dir);
	}

	public LinkedList<Point> findPath(Unit u, Point a, Point b) {
		LinkedList<Point> points = new LinkedList<Point>();

		// if (u.moveType == 2) {
		// points.add(a);
		// points.add(b);
		// return points;
		// }

		HashMap<Point, Point> parents = new HashMap<Point, Point>();
		HashMap<Point, Point2D.Double> scores = new HashMap<Point, Point2D.Double>();
		// g,h

		PriorityQueue<Point> open = new PriorityQueue<Point>(1, new PointComparator<Point>(scores));
		HashSet<Point> closed = new HashSet<Point>();

		open.add(a);
		scores.put(a, new Point2D.Double(0, guess(a.x, a.y, b.x, b.y)));

		while (open.size() > 0 && !closed.contains(b)) {
			Point current = open.poll();
			closed.add(current);

			for (int i = 0; i < 8; i++) {
				Point possible = new Point(current.x + getDX(i), current.y + getDY(i));

				if (possible.x >= 0 && possible.y >= 0 && possible.x < getWidth() && possible.y < getHeight()) {

					if (isWalkable(u, current.x, current.y, i) && !closed.contains(possible)) {
						if (open.contains(possible)) {
							if (scores.get(possible).x > scores.get(current).x + getCost(u, current.x, current.y, i)) {
								scores.get(possible).x = scores.get(current).x + getCost(u, current.x, current.y, i);
								parents.put(possible, current);
							}
						} else {
							parents.put(possible, current);

							scores.put(possible,
									new Point2D.Double(scores.get(current).x + getCost(u, current.x, current.y, i),
											30 * guess(possible.x, possible.y, b.x, b.y)));
							open.offer(possible);
						}
					}
				}
			}
		}

		Point current = b;

		while (!current.equals(a)) {
			points.addFirst(current);
			if (parents.get(current) == null)
				return new LinkedList<Point>();
			current = parents.get(current);
		}

		return points;
	}

	private static class PointComparator<T extends Point> implements Comparator<T> {
		HashMap<Point, Point2D.Double> scores;

		public PointComparator(HashMap<Point, Point2D.Double> scores) {
			this.scores = scores;
		}

		public int compare(T a, T b) {
			if (scores.get(a) == null)
				System.out.println(a + "is null");
			else if (scores.get(b) == null)
				System.out.println(b + "is null");

			return (int) Math.signum(-(scores.get(b).x + scores.get(b).y) + (scores.get(a).x + scores.get(a).y));
		}
	}

	public static int getDir(int x, int y, int imX, int imY) {
		if (imX - x == 0) {
			if (imY - y == 0)
				return -1;
			else if (imY - y > 0)
				return SOUTH;
			else
				return NORTH;
		} else if (imX - x > 0) {
			if (imY - y == 0)
				return EAST;
			else if (imY - y > 0)
				return SOUTH_EAST;
			else
				return NORTH_EAST;
		} else {
			if (imY - y == 0)
				return WEST;
			else if (imY - y > 0)
				return SOUTH_WEST;
			else
				return NORTH_WEST;
		}
	}
}
