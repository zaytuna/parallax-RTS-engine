package framework;

import java.awt.Color;
import java.util.ArrayList;

import math.Matrix;
import math.Vector3D;

public class BodyPart {
	public ArrayList<Integer> types = new ArrayList<Integer>();
	public ArrayList<Integer> widths = new ArrayList<Integer>();
	public ArrayList<ArrayList<Vector3D>> points = new ArrayList<ArrayList<Vector3D>>();
	public ArrayList<Color> colors = new ArrayList<Color>();
	public Matrix rot = Matrix.createIdentityMatrix(3);
	public String name = "";
	public Vector3D position = new Vector3D(0, 0, 0);

	public String toString() {
		return name;
	}
}
