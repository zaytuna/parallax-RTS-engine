package framework;

import java.util.ArrayList;

public class World {
	public Terrain terrain;
	public ArrayList<Unit> units = new ArrayList<Unit>();
	public ArrayList<Effect> effects = new ArrayList<Effect>();
	public ArrayList<Projectile> projectiles = new ArrayList<Projectile>();

	public TerrainLayer<Unit> unitLayer;
	public TerrainLayer<Double> visited;

	public World() {
		terrain = Terrain.fractal(.1, 50, 8);

		// terrain.setLogisticHeightAt(terrain.getWidth() / 2,
		// terrain.getHeight() / 2, 10, 1, 25);

		unitLayer = new TerrainLayer<Unit>(terrain);
		visited = new TerrainLayer<Double>(terrain);

		// visitedTexture = new BufferedImage(terrain.getWidth(),
		// terrain.getHeight(), BufferedImage.TYPE_INT_ARGB);

		visited.fill(new Double(0));
	}

	public void spawnUnit(Unit u) {
		units.add(u);
		u.owner.units.add(u);
	}

	public void addEffect(Effect e) {
		effects.add(e);
	}
}
