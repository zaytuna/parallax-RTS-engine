package framework;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import lowlevel.Methods;
import gui.GameWindow;
import gui.RuntimeOptions;

public class VisibleAreas implements Runnable {
	final int W, H;

	Player owner;
	private BufferedImage mask = new BufferedImage(RuntimeOptions.options.minimapGrain, RuntimeOptions.options.minimapGrain,
			BufferedImage.TYPE_INT_ARGB), mask2 = new BufferedImage(RuntimeOptions.options.minimapGrain,
			RuntimeOptions.options.minimapGrain, BufferedImage.TYPE_INT_ARGB);
	boolean clock = false;

	VisibleAreas(Player p, int w, int h) {
		this.W = w;
		this.H = h;
		this.owner = p;

		new Thread(this).start();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(RuntimeOptions.options.minimapGrain);

				for (int i = 0; i < GameWindow.self.display.world.terrain.getWidth(); i++)
					for (int j = 0; j < GameWindow.self.display.world.terrain.getHeight(); j++) {
						GameWindow.self.display.world.visited.put(GameWindow.self.display.world.visited.get(i, j)
								* (1 - RuntimeOptions.options.sightDecay), i, j);
						getPercentShade(i, j);
					}
				clock = !clock;
				updateMask();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void updateMask() {
		if (mask.getWidth() != RuntimeOptions.options.minimapGrain) {
			mask = new BufferedImage(RuntimeOptions.options.minimapGrain, RuntimeOptions.options.minimapGrain,
					BufferedImage.TYPE_INT_ARGB);
			mask2 = new BufferedImage(RuntimeOptions.options.minimapGrain, RuntimeOptions.options.minimapGrain,
					BufferedImage.TYPE_INT_ARGB);
		}

		for (int i = 0; i < RuntimeOptions.options.minimapGrain; i++)
			for (int j = 0; j < RuntimeOptions.options.minimapGrain; j++) {

				int x = i * GameWindow.self.display.world.terrain.getWidth() / RuntimeOptions.options.minimapGrain, y = j
						* GameWindow.self.display.world.terrain.getHeight() / RuntimeOptions.options.minimapGrain;
				(clock ? mask : mask2).setRGB(i, j, Methods.colorMeld(
						RuntimeOptions.options.background,
						Methods.colorMeld(Methods.getColor(RuntimeOptions.options.visitedFade,
								(int) (200 * RuntimeOptions.options.fogColorChange)), new Color(0, 0, 0, 0),
								getPercentShade(x, y)), GameWindow.self.display.world.visited.get(x, y)).getRGB());
			}
	}

	public ArrayList<Unit> getVisibleEnemyUnits(Unit u) {
		ArrayList<Unit> list = new ArrayList<Unit>();
		for (Unit x : GameWindow.self.display.world.units)
			if (!owner.alliedWith(x.owner)
					&& ((x.y - u.y) * (x.y - u.y) + (x.x - u.x) * (x.x - u.x) <= x.sight * x.sight))
				list.add(x);

		return list;
	}

	public ArrayList<Unit> getVisibleEnemyUnits() {
		ArrayList<Unit> list = new ArrayList<Unit>();
		for (Unit u : GameWindow.self.display.world.units)
			if (!owner.alliedWith(u.owner))
				for (Unit x : owner.units)
					if ((x.y - u.y) * (x.y - u.y) + (x.x - u.x) * (x.x - u.x) <= x.sight * x.sight)
						list.add(u);

		return list;
	}

	// for Fog Of War purposes
	public double getPercentShade(int x, int y) {
		double shade = 0;
		for (int i = 0; i < owner.units.size(); i++)
			shade += Math.max(owner.units.get(i).sight
					- Math.sqrt((owner.units.get(i).x - x) * (owner.units.get(i).x - x) + (owner.units.get(i).y - y)
							* (owner.units.get(i).y - y)), 0)
					/ RuntimeOptions.options.sightFuzz;

		if (shade > 0)
			GameWindow.self.display.world.visited.put(Math.max(Math.max(Math.min(shade, 1), 0),
					GameWindow.self.display.world.visited.get(x, y)), x, y);

		return Math.max(Math.min(shade, 1), 0);
	}

	public static int shadeFogOfWar(int rgb, double percent) {
		// (alpha << 24)|(red << 16)|(green << 8)|blue;
		int r = (rgb >> 16) & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = (rgb >> 0) & 0xff;
		return (255 << 24) | ((int) (percent * (percent * r + (1 - percent) * g + (1 - percent) * b)) << 16)
				| ((int) (percent * ((1 - percent) * r + percent * g + (1 - percent) * b)) << 8)
				| (int) (percent * ((1 - percent) * r + (1 - percent) * g + percent * b));
	}

	public Image getMask() {
		return clock ? mask2 : mask;
	}
}