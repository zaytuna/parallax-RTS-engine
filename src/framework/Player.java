package framework;

import java.awt.*;
import java.util.ArrayList;

public class Player {
	public ArrayList<Player> allies = new ArrayList<Player>();
	public ArrayList<Unit> units = new ArrayList<Unit>();
	public Color color;
	public String name;
	public VisibleAreas va;

	public int coconuts = 100, hapiness = 20;

	public Player(Color c, Point start, int w, int h) {
		color = c;
		va = new VisibleAreas(this, w, h);
	}

	public boolean alliedWith(Player p) {
		return (p == this || allies.contains(p));
	}

	public String toString() {
		return name;
	}

	public java.util.ArrayList<Unit> getVisibleEnemyUnits() {
		return va.getVisibleEnemyUnits();
	}
}