package framework;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;

import math.Vector3D;
import gui.GameWindow;

public class Unit extends Vector3D implements Runnable {// thread is for unit AI
	// and pathfinding
	public static final int NUM_ORDERS = 10;
	public static final String[] ORDER_NAMES = { "Move", "Stop", "Attack", "Patrol", "Build", "Cloak", "Hold", "Cast",
			"Morph", "Fire" };
	public static final int[] ORDER_HOTKEYS = { KeyEvent.VK_M, KeyEvent.VK_S, KeyEvent.VK_A, KeyEvent.VK_P,
			KeyEvent.VK_B, KeyEvent.VK_C, KeyEvent.VK_H, KeyEvent.VK_G, KeyEvent.VK_V, KeyEvent.VK_F };
	public static final int IDLE = 0, MOVING = 1, ATTACKING = 2, HOLDING = 3, SEARCHING = 4, PATROLING = 5,
			MORPHING = 6, DEAD = -1;

	public static final Unit[] ALL_UNITS = { new Unit(15, 5, 1, 50, 10, .5, 0, 20, 0, 2, 0, 1, 2, 3, 4, 104, 204, 304),
			new Unit(20, 10, 1, 100, 5, 2, 0, 50, 0, .4, 0, 1, 2, 3, 6),
			new Unit(15, 100, 10, 300, 20, .3, 0, 400, 0, 8, 0, 1, 2, 3, 6, 9),
			new Unit(20, 30, 4, 120, 8, 1.5, 0, 150, 2, 3, 0, 1, 2, 3, 6, 9) };
	public static final String[] UNIT_NAMES = { "Worker", "Scout", "Tank", "Plane" };
	// end of public static final vars

	// actual stats for units
	public double sight, attack, armor, health, cooldown, speed, stamina, range, dh = 0;
	// recharge is current % of cooldown done
	public double maxSight, maxAttack, maxArmor, maxHealth, maxCooldown, maxSpeed, maxStamina, maxRange,
			versitility = 1, bulletTime = 100;
	public int[] orders;
	boolean explosiveRounds = true, homingRounds = false, cloaked = false;

	public int state = IDLE;

	/*
	 * ORDER MAPPING: (mod 100)
	 * 0: move
	 * 1: stop
	 * 2: attack
	 * 3: patrol
	 * 4: build >>divby100 to get unit
	 * 5: cloak = !cloak
	 * 6: hold position
	 * 7: cast spell >>divby100 to get spell
	 * 8: transform >>divby100 to get unit
	 * 9: fire
	 */

	public int unitType, moveType = 0, imX, imY;
	public double recharge, toX, toY, homeX, homeY, awayX, awayY, distanceFromGround, vdfg;
	public Player owner;

	public ArrayList<BodyPart> visual = new ArrayList<BodyPart>();

	public LinkedList<Point> path = new LinkedList<Point>();

	double vx, vy, ax, ay;

	private Unit(double sight, double attack, double armor, double health, double cooldown, double speed,
			double stamina, double range) {
		this(sight, attack, armor, health, cooldown, speed, stamina, range, 0, 1, 0, 1, 2, 3, 4, 5, 6, 7);
	}

	private Unit(double sight, double attack, double armor, double health, double cooldown, double speed,
			double stamina, double range, int moveType, double versitility, int... orders) {
		super(0, 0, 0);
		this.sight = sight;
		this.attack = attack;
		this.health = health;
		this.armor = armor;
		this.cooldown = cooldown;
		this.speed = speed;
		this.stamina = stamina;
		this.range = range;

		this.maxSight = sight;
		this.maxAttack = attack;
		this.maxHealth = health;
		this.maxArmor = armor;
		this.maxCooldown = cooldown;
		this.maxSpeed = speed;
		this.maxStamina = stamina;
		this.maxRange = range;
		this.versitility = versitility;
		this.moveType = moveType;

		this.orders = orders;

		if (moveType == 2)
			distanceFromGround = 10;
		else
			distanceFromGround = 0;
	}

	public static Unit createFromOwner(Player p, int type) {
		Unit u = ALL_UNITS[type].clone();
		u.owner = p;
		u.unitType = type;
		return u;
	}

	public void run() {
		path = GameWindow.self.display.world.terrain.findPath(this, new Point((int) x, (int) y), new Point((int) toX,
				(int) toY));
	}

	public void teleportTo(int x, int y) {
		this.x = x;
		this.y = y;
		imX = x;
		imY = y;
	}

	public void goTo(int x, int y) {
		toX = x;
		toY = y;
		imX = (int) this.x;
		imY = (int) this.y;
		new Thread(this).start();

	}

	@Override
	public Unit clone() {
		return new Unit(sight, attack, health, armor, cooldown, speed, stamina, range, moveType, versitility, orders);
	}

	public void update() {
		if (state == SEARCHING) {

		}
		double distance = Math.sqrt((imX - x) * (imX - x) + (imY - y) * (imY - y));
		vdfg += (distanceFromGround + GameWindow.self.display.world.terrain.getHeightAt((int) x, (int) y) - z) / 30;
		vdfg *= .9;
		z += vdfg;

		while (path != null && !path.isEmpty() && (distance <= speed)) {
			Point p = path.poll();

			for (int q = -2; q <= 2; q++)
				for (int r = -2; r <= 2; r++)
					if (imX + q > 0 && imX + q < GameWindow.self.display.world.terrain.getWidth() && imY + r > 0
							&& imY + r < GameWindow.self.display.world.terrain.getHeight())
						GameWindow.self.display.world.unitLayer.put(null, imX + q, imY + r);

			imX = p.x;
			imY = p.y;

			GameWindow.self.display.world.unitLayer.put(this, imX, imY);

			distance = Math.sqrt((imX - x) * (imX - x) + (imY - y) * (imY - y));
		}

		if (distance > speed) {
			double cost = 80;

			if (moveType != 2)
				try {
					cost = GameWindow.self.display.world.terrain.getCost(this, (int) (x), (int) (y), Terrain.getDir(
							(int) x, (int) y, imX, imY));
				} catch (Exception e) {
				}

			x += 18 * speed * (imX - x) / (distance * cost);
			y += 18 * speed * (imY - y) / (distance * cost);
		} else {
			switch (state) {
			case IDLE:
				break;
			case MOVING:
				state = IDLE;
				break;
			case PATROLING:
				if (Math.abs(homeX - x) <= speed + 1 && Math.abs(homeY - y) <= speed + 1)
					goTo((int) awayX, (int) awayY);
				else
					goTo((int) homeX, (int) homeY);

				break;
			}
		}
	}

	public int getRGB() {
		return (255 << 24) | (Math.abs((int) ((10 * attack / cooldown + range) % 255)) << 16)
				| ((int) Math.abs(20 * armor + (int) (2 * Math.log(health)) % 255) << 8)
				| (int) Math.abs((40 * sight * speed + stamina) % 255);
	}

	public static boolean needsMenu(int orderType) {
		return (orderType % 100 == 4 || orderType % 100 == 7 || orderType % 100 == 8);
	}

	public static boolean needsPoint(int orderType) {
		return (orderType % 100 == 0 || orderType % 100 == 2 || orderType % 100 == 3 || orderType % 100 == 4
				|| orderType % 100 == 7 || orderType % 100 == 9);
	}

	public void perform(int order, double mx, double my) {
		switch (order % 100) {
		case 0:
			goTo((int) mx, (int) my);
			state = MOVING;
			break;
		case 1:
			imX = (int) x;
			imY = (int) y;
			path.clear();
			state = IDLE;
			break;
		case 2:
			goTo((int) mx, (int) my);
			state = SEARCHING;
			break;
		case 3:
			homeX = x;
			homeY = y;
			awayX = mx;
			awayY = my;
			goTo((int) mx, (int) my);
			state = PATROLING;
			break;
		case 4:
			System.out.println("You ordered " + (order / 100) + "?");
			Unit u = Unit.createFromOwner(owner, order / 100);
			u.teleportTo((int) mx, (int) my);
			GameWindow.self.display.world.spawnUnit(u);
			break;
		case 9:
			Projectile proj = new Projectile(this, new Vector3D(mx, my, GameWindow.self.display.world.terrain
					.getHeightAt((int) mx, (int) my)));

			GameWindow.self.display.world.projectiles.add(proj);

			break;
		}
	}
}
