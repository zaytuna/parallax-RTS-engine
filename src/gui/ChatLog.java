package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class ChatLog implements Runnable {
	List<Message> messages = new ArrayList<Message>();
	PriorityQueue<Message> waiting = new PriorityQueue<Message>();
	Message lastMess;

	FontMetrics fm;

	public ChatLog(FontMetrics fm) {
		this.fm = fm;
		new Thread(this).start();
	}

	public void paintComponent(Graphics g) {
		g.setFont(new Font("SANS_SERIF", Font.PLAIN, 36));// "OLD ENGLISH TEXT MT"
		fm = g.getFontMetrics();

		for (int i = 0; i < messages.size(); i++) {
			g.setColor(messages.get(i).color);
			g.drawString(messages.get(i).text, messages.get(i).pixelAt, 35);
		}
	}

	public void newString() {
		if (waiting.size() > 0) {
			lastMess = waiting.poll();
			messages.add(lastMess);
		}
	}

	public void addMessage(String s, Color c, double speed, int priority) {
		Message v = new Message(s, fm.stringWidth(s), GameDriver.screen.width);
		v.color = c;
		v.speed = speed;
		v.priority = priority;
		waiting.offer(v);
	}

	public void add(String text) {
		addMessage(text, Color.ORANGE, 4, 5);
	}

	public class Message implements Comparable<Message> {
		String text;
		double fade = 0, speed = 1;
		int pixelAt, width, priority = 5;
		Color color;

		Message(String s, int width, int pixelAt) {
			this.text = s;
			this.width = width;
			this.pixelAt = pixelAt;
		}

		@Override
		public int compareTo(Message m) {
			return m.priority - priority;
		}
	}

	public void run() {

		while (true) {
			try {
				Thread.sleep(20);
			} catch (InterruptedException exc) {
			}

			for (int i = 0; i < messages.size(); i++) {
				if (messages.get(i).pixelAt + fm.stringWidth(messages.get(i).text) < 0)
					messages.remove(i--);
				else
					messages.get(i).pixelAt -= messages.get(i).speed;
			}

			if (lastMess == null
					|| (fm != null && lastMess.pixelAt + fm.stringWidth(lastMess.text) + 40 < GameDriver.screen.width))
				newString();

		}

	}

}
