package gui;

import framework.Terrain;

import java.awt.Color;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class RuntimeOptions {
	public static RuntimeOptions options;
	public static int terrainSmooth = 1;
	public double sightFuzz = 8;
	public boolean fogOWar = true;
	public boolean shadows = true;
	public double ambientLight = .2;
	public int smooth = 1;
	public int radius = 80;
	public Color unseenFog = Color.black;
	public Color visitedFade = Color.white;
	public Color foreground = Color.WHITE;
	public Color background = Color.BLACK;

	public double fogGray = .4;
	public double fogColorChange = .1;
	public int baseRectWidth = 100;
	public double cameraSpeed = 1;
	public int minimapGrain = 200;
	public double sightDecay = 0;
	public int colorAlpha = 255;
	public boolean cursor = true;
	public double sunSpeed = .01;
	public int cursorSize = 20;
	public int renderMode = 0;
	public boolean wireframe = false;

	public RuntimeOptions() {
		options = this;
	}

	public String handleCommand(String s) {
		try {
			s = s.replaceFirst("[*]", "");
			if (s.contains("=")) {
				String[] split = s.split("=");
				for (Field f : getClass().getDeclaredFields())
					if (f.getName().equalsIgnoreCase(split[0].trim())) {
						split[1] = split[1].trim();
						if (f.getType().equals(int.class))
							f.set(this, Integer.parseInt(split[1]));
						else if (f.getType().equals(boolean.class))
							f.set(this, split[1].startsWith("t"));
						else if (f.getType().equals(double.class))
							f.set(this, Double.parseDouble(split[1]));
						else if (f.getType().equals(Color.class))
							f.set(this, new Color(Integer.parseInt(split[1], 16)));
						else
							return "! Could not figure out how to input type " + f.getType();
						return "SUCCESSFUL SET OPPERATION";
					}
				return "can't figure out what the field '" + split[0] + "' is";

			} else {
				if (s.equalsIgnoreCase("help"))
					return "use something of the form 'field = value', 'get value', or say 'fields' for more info";
				if (s.equalsIgnoreCase("fields"))
					return feilds();
				else if (s.startsWith("get")) {
					s = s.replaceFirst("get", "").trim();
					for (Field f : getClass().getDeclaredFields())
						if (f.getName().equalsIgnoreCase(s))
							return f.getName() + " is " + f.get(this).toString();
					return "! Could not find field " + s;

				} else if (s.equalsIgnoreCase("terrain help")) {
					String str = "<html>Here are the methods in terrain: ";
					for (Method m : Terrain.class.getDeclaredMethods()) {
						Type[] parT = m.getGenericParameterTypes();
						String params = "";
						for (int i = 0; i < parT.length; i++)
							params += ", " + parT[i].toString();
						params = params.replaceFirst(", ", "");

						str += "<BR>" + m.getName() + "(" + params + ")";
					}

					return str + "</html>";

				} else if (s.startsWith("terrain.")) {
					s = s.replaceFirst("terrain.", "").trim();

					if (s.contains("(") && s.contains(")")) {
						String[] params = s.substring(s.indexOf("(") + 1, s.indexOf(")")).split(",");
						s = s.substring(0, s.indexOf("("));

						for (Method m : Terrain.class.getDeclaredMethods())
							if (m.getName().equalsIgnoreCase(s.trim())) {
								Type[] parT = m.getGenericParameterTypes();
								if (parT.length != params.length && parT.length != 0) {
									System.out.println(parT.length + "!=" + params.length + "... " + m.getName());
									continue;
								}
								Object[] args = new Object[parT.length];
								for (int i = 0; i < parT.length; i++) {
									if (parT[i].equals(int.class))
										args[i] = Integer.parseInt(params[i]);
									else if (parT[i].equals(boolean.class))
										args[i] = params[i].startsWith("t");
									else if (parT[i].equals(double.class))
										args[i] = Double.parseDouble(params[i]);
									else if (parT[i].equals(Color.class))
										args[i] = new Color(Integer.parseInt(params[i], 16));
									else
										return "! Could not figure out how to input type " + parT[i];
								}
								GameWindow.self.display.lighter.waiting = true;
								Thread.sleep(10);
								Object o = m.invoke(GameWindow.self.display.world.terrain, args);
								if (o == null) {
									GameWindow.self.display.lighter.waiting = false;
									return "Success!";
								}

								if (o instanceof Terrain) {

									GameWindow.self.display.world.terrain = ((Terrain) o);
									GameWindow.self.display.lighter.t = ((Terrain) o).clone();
									GameWindow.self.display.lighter.waiting = false;

									GameWindow.self.display.world.unitLayer.resize((Terrain) o, null);
									GameWindow.self.display.world.visited.resize((Terrain) o, 0D);

									System.out.println(GameWindow.self.display.world.unitLayer.model.getWidth());

									return "New terrain!";
								}
								GameWindow.self.display.lighter.waiting = false;
								return "Return value of " + s + ": " + o.toString();
							}
						return "Couldn't find terrain method '" + s + "'";
					} else if (s.contains("help")) {
						s = s.replaceAll("help", "").trim();
						String str = "<html>";
						for (Method m : Terrain.class.getDeclaredMethods())
							if (m.getName().equalsIgnoreCase(s))
								str += m.toGenericString() + "<BR>";
						return str + "</html>";
					}

				}
			}
		} catch (Exception e) {
			System.out.println("ERROR IN COMMAND PARSE");
			return "! ERROR: " + e.getMessage();
		}

		return "! Did not understand '" + s + "'";
	}

	public String feilds() {
		String s = "<html>Here are the properties that are easy to change: ";
		for (Field f : getClass().getDeclaredFields())
			s += "<BR>" + f.getName();

		return s + "</html>";
	}
}
