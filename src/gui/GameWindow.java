package gui;

import framework.Player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lowlevel.Methods;

public class GameWindow extends JFrame {
	private static final long serialVersionUID = 1903142717890981086L;
	public static GameWindow self = new GameWindow();

	Player me;

	public GamePanel display;
	JTextField enter = new JTextField();
	JLabel out = new JLabel();
	JPanel bot;

	private GameWindow() {
		setLayout(new BorderLayout());
		new RuntimeOptions();
		me = new Player(new Color(150, 0, 0), new Point(0, 0), 200, 200);
		display = new GamePanel(me);
		// try {
		// setCursor(Toolkit.getDefaultToolkit().createCustomCursor(ImageIO.read(new
		// File("Resources/cursor4.png")),
		// new Point(0, 0), "cursor"));
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		bot = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 5));
		bot.add(enter);
		bot.add(out);
		out.setForeground(Color.pink);
		bot.setBackground(Methods.colorMeld(RuntimeOptions.options.background, Color.GRAY, 0.3));

		enter.setBackground(RuntimeOptions.options.background);
		enter.setPreferredSize(new Dimension(600, 35));
		enter.setCaretColor(Color.ORANGE);
		enter.setForeground(Color.CYAN);
		enter.setHorizontalAlignment(JTextField.CENTER);
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (enter.getText().startsWith("*")) {
					String str = RuntimeOptions.options.handleCommand(enter.getText());
					out.setText(str);
					out.setForeground(str.startsWith("!") ? Color.RED : new Color(200, 255, 200));
				} else {
					display.chat.add(enter.getText());

					display.requestFocus();
					bot.setVisible(false);
					enter.setText("");
					out.setText("");
				}
			}
		});
		enter.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
					display.requestFocus();
					bot.setVisible(false);
					enter.setText("");
					out.setText("");
				}
			}
		});

		addKeyListener(display);
		add(display, BorderLayout.CENTER);
		add(bot, BorderLayout.SOUTH);

		bot.setVisible(false);
	}
}
