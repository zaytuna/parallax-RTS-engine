package gui;

import static gui.RuntimeOptions.options;
import framework.Player;
import framework.Terrain;
import framework.Unit;
import framework.World;

import java.awt.AWTException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import javax.swing.JPanel;

import lowlevel.Methods;
import lowlevel.PixelDrawer;
import math.Vector2D;

public class GamePanel extends JPanel implements Runnable, MouseListener, MouseMotionListener, MouseWheelListener,
		KeyListener {
	private static final long serialVersionUID = -3019692380076786489L;
	public static final double K = 100;

	int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;

	PixelDrawer graphics = new PixelDrawer(GameDriver.screen.width, getHeight(), this);
	public World world = new World();

	Set<Integer> down = new HashSet<Integer>();

	ArrayList<Unit> selected = new ArrayList<Unit>();
	Point startDrag, endDrag;
	Player player;

	double mx, my, mz;

	int perspectiveX, perspectiveY, paintFPS, drawFPS, lightFPS, counterPaint, counterDraw, counterLight, grain = 1,
			menuState = -999, pendingOrder = -1;

	// for menuState: 0 is main order menu, > 0 indicates the specific inner
	// menu, -999 indicates none selected, and -1 indicates choose point.

	public double height, distance = 30, vh;
	BufferedImage terrainImage = new BufferedImage(GameDriver.screen.width, GameDriver.screen.height,
			BufferedImage.TYPE_INT_ARGB), offScreen = new BufferedImage(GameDriver.screen.width,
			GameDriver.screen.height, BufferedImage.TYPE_INT_ARGB);

	long lastPaintUpdateTime, lastTerrainDrawUpdateTime, lastLightUpdateTime;
	ChatLog chat;

	boolean clock = true;// alternates every frame, used for manually
	// double-buffering terrain img
	Point cursor = new Point(400, 400);
	Robot robot;

	LightingUpdateThread lighter;

	MiniMap minimap;

	public GamePanel(Player me) {
		super(true);

		setBackground(Color.black);
		setDoubleBuffered(true);

		player = me;

		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);

		setDoubleBuffered(true);
		setIgnoreRepaint(true);

		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}

		height = world.terrain.maxHeight() + 10;

		minimap = new MiniMap(this, world);

		Thread gpt = new Thread(this, "GamePanel Thread");
		gpt.setPriority(10);
		gpt.start();
		new Thread(new UnitUpdateThread()).start();

		robot.mouseMove(GameDriver.screen.width / 2, GameDriver.screen.height / 2);

		lighter = new LightingUpdateThread();
		new Thread(lighter).start();

		world.visited.fill(0D);
	}

	public void update() {
		minimap.updateImage();
		grain = Math.max((int) (Math.abs(distance) / 100) + 1, 1);

		createTerrainImage();
		clock = !clock;

		// for (Effect effect : world.effects)
		// effect.draw(graphics, GamePanel.this);

		// graphics.update();
		// graphics.clear(0);

		if (counterPaint++ % 10 == 0) {
			counterPaint %= 10;
			if (System.currentTimeMillis() - lastPaintUpdateTime == 0)
				paintFPS = 1000;
			else
				paintFPS = (int) (10000 / (System.currentTimeMillis() - lastPaintUpdateTime));
			lastPaintUpdateTime = System.currentTimeMillis();
		}
	}

	private class UnitUpdateThread implements Runnable {
		public void run() {
			while (true) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				for (Unit u : world.units)
					u.update();

			}
		}
	}

	public class LightingUpdateThread implements Runnable {
		Terrain t = world.terrain.clone();
		boolean waiting = false;

		public void run() {
			while (true) {
				do
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				while (waiting);

				if (counterLight++ % 10 == 0) {
					counterLight %= 10;
					if (System.currentTimeMillis() - lastLightUpdateTime == 0)
						lightFPS = 1000;
					else
						lightFPS = (int) (10000 / (System.currentTimeMillis() - lastLightUpdateTime));
					lastLightUpdateTime = System.currentTimeMillis();
				}
				t.sunDir.incrementAngle(RuntimeOptions.options.sunSpeed);

				minX = Integer.MAX_VALUE;
				minY = Integer.MAX_VALUE;
				maxX = Integer.MIN_VALUE;
				maxY = Integer.MIN_VALUE;

				lightView(t, grain, true, true, perspectiveX, perspectiveY);
				lightView(t, grain, true, false, perspectiveX, perspectiveY);
				lightView(t, grain, false, false, perspectiveX, perspectiveY);
				lightView(t, grain, false, true, perspectiveX, perspectiveY);

				t.smoothColors(RuntimeOptions.options.smooth, minX, minY, maxX, maxY, 1);

				world.terrain.setColors(t, minX, minY, maxX, maxY);
			}
		}
	}

	public void lightView(Terrain t, int grain, boolean xDir, boolean yDir, int px, int py) {
		boolean cont = true;
		// System.out.println(grain);
		for (int i = grain * ((px / Terrain.GRAIN + (xDir ? 1 : 0)) / grain); cont
				&& (xDir ? (i < world.terrain.getWidth()) : (i >= 0)); i += xDir ? grain : -grain) {
			if (i < 0 || i >= world.terrain.getWidth())
				continue;
			cont = false;
			for (int j = grain * ((py / Terrain.GRAIN + (yDir ? 1 : 0)) / grain); yDir ? ((j * Terrain.GRAIN - py) * K
					/ (height - world.terrain.getHeightAt(i, j)) < getHeight() / 2 + 100) : (j * Terrain.GRAIN - py)
					* K / (height - world.terrain.getHeightAt(i, j)) > -getHeight() / 2 - 100
					&& (yDir ? (j < world.terrain.getHeight()) : (j >= 0)); j += (yDir ? grain : -grain)) {
				if (j < 0 || j >= world.terrain.getHeight())
					continue;

				// ***************** begin light ***************
				t.updateShadow(i, j);
				t.updateColorAndLight(i, j);

				// ***************** end light ****************

				int nextRowX = (int) ((i * Terrain.GRAIN - perspectiveX) * K / (height - world.terrain
						.getHeightAt(i, j)));
				cont = cont
						|| (xDir ? (nextRowX < GameDriver.screen.width / 2 + 100)
								: (nextRowX > -GameDriver.screen.width / 2 - 100));

				if (j < minY)
					minY = j;
				if (j > maxY)
					maxY = j;
			}
			if (i < minX)
				minX = i;
			if (i > maxX)
				maxX = i;
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(8);
			} catch (Exception e) {
			}
			if (startDrag == null) {
				if (cursor.x == 0)
					perspectiveX -= distance * RuntimeOptions.options.cameraSpeed / 4;
				else if (cursor.x == GameDriver.screen.width - 1)
					perspectiveX += distance * RuntimeOptions.options.cameraSpeed / 4;

				if (cursor.y == 0)
					perspectiveY -= distance * RuntimeOptions.options.cameraSpeed / 4;
				else if (cursor.y == GameDriver.screen.height - 1)
					perspectiveY += distance * RuntimeOptions.options.cameraSpeed / 4;

				try {
					vh += ((world.terrain.getHeightAt(perspectiveX / Terrain.GRAIN, perspectiveY / Terrain.GRAIN) + distance) - height) / 60;

					vh *= .9;
					height += vh;
				} catch (IndexOutOfBoundsException e) {

				}
			}

			// for (Effect effect : world.effects)
			// effect.update();
			//
			// for (Projectile p : world.projectiles)
			// p.update();

			if (down.contains(KeyEvent.VK_SPACE) && minimap.getSX() < GameDriver.screen.width / 2) {
				minimap.setSX(minimap.getSX() + (GameDriver.screen.width / 2 - minimap.getSX()) / 4);
				minimap.setSY(minimap.getSY() + (getHeight() / 2 - minimap.getSY()) / 4);
			} else {
				minimap.setSX(minimap.getSX() + (200 - minimap.getSX()) / 4);
				minimap.setSY(minimap.getSY() + (200 - minimap.getSY()) / 4);
			}

			update();
			repaint();
		}
	}

	public BufferedImage createTerrainImage() {
		Graphics2D g = (Graphics2D) (clock ? offScreen : terrainImage).getGraphics();

		g.setColor(RuntimeOptions.options.background);
		g.fillRect(0, 0, GameDriver.screen.width, getHeight());

		int px = perspectiveX, py = perspectiveY;

		terrainDraw(g, grain, true, true, cursor, px, py);
		terrainDraw(g, grain, true, false, cursor, px, py);
		terrainDraw(g, grain, false, false, cursor, px, py);
		terrainDraw(g, grain, false, true, cursor, px, py);

		g.dispose();

		return (clock ? offScreen : terrainImage);
	}

	private abstract class PaintShape {
		public abstract void paint(Graphics g, boolean b);
	}

	private class PaintSquare extends PaintShape {
		public Color c;
		public int x, y, w, h;

		PaintSquare(int x, int y, int w, int h, Color c) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
			this.c = c;
		}

		@Override
		public void paint(Graphics g, boolean b) {
			g.setColor(c);
			if (b)
				g.drawRect(x, y, Math.min(w, GameDriver.screen.width / 2), Math.min(h, GameDriver.screen.height / 2));
			else
				g.fillRect(x, y, Math.min(w, GameDriver.screen.width / 2), Math.min(h, GameDriver.screen.height / 2));
		}
	}

	private class PaintTriangle extends PaintShape {
		Color c;
		int x1, x2, x3, y1, y2, y3;

		PaintTriangle(int x1, int y1, int x2, int y2, int x3, int y3, Color c) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.x3 = x3;
			this.y3 = y3;
			this.c = c;
		}

		@Override
		public void paint(Graphics g, boolean b) {
			g.setColor(c);
			// g.fillOval(x1, x2, 10, 10);
			if (b)
				g.drawPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
			else
				g.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
		}

	}

	public void terrainDraw(Graphics g, int grain, boolean xDir, boolean yDir, Point mouse, int px, int py) {
		boolean cont = true;

		Stack<PaintShape> shapeStack = new Stack<PaintShape>();

		for (int i = grain * ((px / Terrain.GRAIN + (xDir ? 1 : 0)) / grain); cont
				&& (xDir ? (i < world.terrain.getWidth()) : (i >= 0)); i += xDir ? grain : -grain) {
			if (i < 0 || i >= world.terrain.getWidth())
				continue;
			cont = false;
			for (int j = grain * ((py / Terrain.GRAIN + (yDir ? 1 : 0)) / grain); yDir ? ((j * Terrain.GRAIN - py) * K
					/ (height - world.terrain.getHeightAt(i, j)) < getHeight() / 2 + 100) : (j * Terrain.GRAIN - py)
					* K / (height - world.terrain.getHeightAt(i, j)) > -getHeight() / 2 - 100
					&& (yDir ? (j < world.terrain.getHeight()) : (j >= 0)); j += (yDir ? grain : -grain)) {
				if (j < 0 || j >= world.terrain.getHeight())
					continue;

				// ***************** begin draw square*****************

				Vector2D posA = screenPos(i, j, world.terrain.getHeightAt(i, j));

				if (options.renderMode == 0) {
					double w = Terrain.GRAIN * RuntimeOptions.options.baseRectWidth
							* (1 + 1.5 * Math.abs(posA.x - GameDriver.screen.width / 2) / GameDriver.screen.width)
							/ (height - world.terrain.getHeightAt(i, j)) + 1;
					double h = Terrain.GRAIN * RuntimeOptions.options.baseRectWidth
							* (1 + 1.5 * Math.abs(posA.y - getHeight() / 2) / getHeight())
							/ (height - world.terrain.getHeightAt(i, j)) + 1;

					if (RuntimeOptions.options.fogOWar) {
						double p = player.va.getPercentShade(i, j);

						if (p > 0 || world.visited.get(i, j) > 0) {
							Color other = Methods.colorMeld(RuntimeOptions.options.background, Methods.colorMeld(
									Methods.toGray(world.terrain.getColor(i, j), RuntimeOptions.options.fogGray),
									RuntimeOptions.options.visitedFade, RuntimeOptions.options.fogColorChange),
									world.visited.get(i, j));

							other = Methods.colorMeld(other, world.terrain.getColor(i, j), p);

							shapeStack.push(new PaintSquare((int) (posA.x - w / 2), (int) (posA.y - h / 2), grain
									* (int) w + 1, grain * (int) h + 1, Methods.getColor(other,
									RuntimeOptions.options.colorAlpha)));
						}
					} else {
						shapeStack.push(new PaintSquare((int) (posA.x - w / 2), (int) (posA.y - h / 2), grain * (int) w
								+ 1, grain * (int) h + 1, Methods.getColor(world.terrain.getColor(i, j),
								RuntimeOptions.options.colorAlpha)));
					}
				} else if (options.renderMode == 1 || options.renderMode == 2 || options.renderMode == 3) {
					int ip = Math.min(i + grain, world.terrain.getWidth() - 1), jp = Math.min(j + grain, world.terrain
							.getHeight() - 1);
					Vector2D posC = screenPos(ip, jp, world.terrain.getHeightAt(ip, jp));
					Vector2D posB = screenPos(ip, j, world.terrain.getHeightAt(ip, j));
					Vector2D posD = screenPos(i, jp, world.terrain.getHeightAt(i, jp));

					if (RuntimeOptions.options.fogOWar) {
						double p = player.va.getPercentShade(i, j);

						if (p > 0 || world.visited.get(i, j) > 0) {
							Color colorIJ = Methods.colorMeld(Methods.colorMeld(RuntimeOptions.options.background,
									Methods.colorMeld(Methods.toGray(world.terrain.getColor(i, j),
											RuntimeOptions.options.fogGray), RuntimeOptions.options.visitedFade,
											RuntimeOptions.options.fogColorChange), world.visited.get(i, j)),
									world.terrain.getColor(i, j), p);
							Color colorIPJP = Methods.colorMeld(Methods.colorMeld(RuntimeOptions.options.background,
									Methods.colorMeld(Methods.toGray(world.terrain.getColor(ip, jp),
											RuntimeOptions.options.fogGray), RuntimeOptions.options.visitedFade,
											RuntimeOptions.options.fogColorChange), world.visited.get(ip, jp)),
									world.terrain.getColor(ip, jp), p);

							if (options.renderMode == 3) {
								shapeStack.push(new PaintSquare((int) posA.x, (int) posA.y, (int) Math.abs(posB.x
										- posA.x), (int) Math.abs(posD.y - posA.y), colorIJ));
							} else {
								shapeStack.push(new PaintTriangle((int) posA.x, (int) posA.y, (int) posB.x,
										(int) posB.y, (int) posD.x, (int) posD.y, Methods.getColor(colorIJ,
												options.colorAlpha)));
								if (options.renderMode == 1)
									shapeStack.push(new PaintTriangle((int) posB.x, (int) posB.y, (int) posD.x,
											(int) posD.y, (int) posC.x, (int) posC.y, Methods.getColor(colorIPJP,
													options.colorAlpha)));
							}
						}
					} else {
						if (options.renderMode == 3) {
							shapeStack.push(new PaintSquare((int) posA.x, (int) posA.y,
									(int) Math.abs(posB.x - posA.x), (int) Math.abs(posD.y - posA.y), Methods.getColor(
											world.terrain.getColor(i, j), options.colorAlpha)));
						} else {
							shapeStack.push(new PaintTriangle((int) posA.x, (int) posA.y, (int) posB.x, (int) posB.y,
									(int) posD.x, (int) posD.y, Methods.getColor(world.terrain.getColor(i, j),
											options.colorAlpha)));
							if (options.renderMode == 1)
								shapeStack.push(new PaintTriangle((int) posB.x, (int) posB.y, (int) posD.x,
										(int) posD.y, (int) posC.x, (int) posC.y, Methods.getColor(world.terrain
												.getColor(ip, jp), options.colorAlpha)));
						}
					}
				}

				Vector2D screenPosM = screenPos(mx, my, mz);
				if (Math.abs(posA.x - mouse.x) < Math.abs(screenPosM.x - mouse.x)
						&& Math.abs(posA.y - mouse.y) < Math.abs(screenPosM.y - mouse.y)) {
					mx = i;
					my = j;
					mz = world.terrain.getHeightAt(i, j);
				}

				// ***************** end draw square*****************

				int nextRowX = (int) ((i * Terrain.GRAIN - px) * K / (height - world.terrain.getHeightAt(i, j)));
				cont = cont
						|| (xDir ? (nextRowX < GameDriver.screen.width / 2 + 100)
								: (nextRowX > -GameDriver.screen.width - 100));
			}
		}

		while (!shapeStack.isEmpty())
			shapeStack.pop().paint(g, options.wireframe);
	}

	public static Color inverse(Color c) {
		return new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
	}

	public Vector2D screenPos(double x, double y, double z) {
		double dx = Terrain.GRAIN * x - perspectiveX, dy = Terrain.GRAIN * y - perspectiveY;

		dx *= K / (height - z);
		dy *= K / (height - z);

		return new Vector2D((int) (GameDriver.screen.width / 2 + dx), (int) (getHeight() / 2 + dy));
	}

	public Vector2D worldPos(int x, int y, double z) {
		int dx = x - GameDriver.screen.width / 2, dy = y - getHeight() / 2;

		dx /= K / (height - z);
		dy /= K / (height - z);

		return new Vector2D((dx + perspectiveX) / Terrain.GRAIN, dy + perspectiveY / Terrain.GRAIN);
	}

	public void paintComponent(Graphics go) {
		if (chat == null)
			chat = new ChatLog(getGraphics().getFontMetrics());

		Graphics2D g = (Graphics2D) go;

		if (counterDraw++ % 10 == 0) {
			counterDraw %= 10;
			if (System.currentTimeMillis() - lastTerrainDrawUpdateTime == 0)
				drawFPS = 1000;
			else
				drawFPS = (int) (10000 / (System.currentTimeMillis() - lastTerrainDrawUpdateTime));
			lastTerrainDrawUpdateTime = System.currentTimeMillis();
		}

		// g.drawImage(world.terrain.getImage(), 0, 0, GameDriver.screen.width,
		// getHeight(),
		// perspectiveX / 10, perspectiveY / 10, perspectiveX / 10 +
		// GameDriver.screen.width / 10, perspectiveY
		// / 10 + getHeight() / 10, this);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.drawImage((clock ? terrainImage : offScreen), 0, 0, this);

		g.setStroke(new BasicStroke(5));
		g.setColor(new Color(0, 150, 0));

		if (menuState == -1)
			g.drawOval(cursor.x - 40, cursor.y - 20, 80, 40);

		g.setColor(new Color(0, 0, 190, 100));
		if (startDrag != null && endDrag != null) {
			g.setStroke(new BasicStroke(5));
			g.fillRoundRect(Math.min(startDrag.x, endDrag.x), Math.min(startDrag.y, endDrag.y), Math.max(startDrag.x,
					endDrag.x)
					- Math.min(startDrag.x, endDrag.x), Math.max(startDrag.y, endDrag.y)
					- Math.min(startDrag.y, endDrag.y), 50, 50);

		}

		Set<Integer> notOrders = new HashSet<Integer>();

		int selectedUnitCounter = 0;
		for (Unit u : selected) {
			g.setColor(new Color(u.getRGB()));
			g.fillRect(GameDriver.screen.width - (1 + selectedUnitCounter % 10) * 50,
					5 + 50 * (selectedUnitCounter++ / 10), 45, 45);

			g.setColor(Color.BLUE);
			g.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 1, new float[] { 10 }, 0));
			Vector2D last = new Vector2D(u.x, u.y);

			for (int i = 0; i < u.path.size(); i++) {
				Point p = u.path.get(i);
				Vector2D a = screenPos(last.x, last.y, world.terrain.getHeightAt((int) last.x, (int) last.y)), b = screenPos(
						p.x, p.y, world.terrain.getHeightAt(p.x, p.y));
				g.drawLine((int) a.x, (int) a.y, (int) b.x, (int) b.y);
				last = new Vector2D(p.x, p.y);
			}
			int w = (int) (800 * Math.cbrt(u.health) / (height - u.z));

			g.setColor(Color.GREEN);
			g.setStroke(new BasicStroke(4));

			Vector2D p = screenPos(u.x, u.y, u.z);
			g.drawOval((int) (p.x - w), (int) (p.y - w), (int) (2 * w), (int) (2 * w));
			g.setColor(new Color(0, 255, 0, 140));

			for (int i = 0; i < Unit.NUM_ORDERS; i++)
				if (!Methods.containsMod(u.orders, i, 100) && !notOrders.contains(i % 100))
					notOrders.add(i % 100);
		}

		for (Unit u : world.units) {
			Vector2D p2 = screenPos(u.x, u.y, u.z);
			int w2 = (int) (1200 * Math.cbrt(u.health) / (height - u.z));
			if (u.moveType == 2) {
				Vector2D p1 = screenPos(u.x, u.y, u.z - u.distanceFromGround);
				int w1 = 10;
				g.setColor(Color.BLUE);
				g.fillOval((int) p1.x - w1 / 2, (int) p1.y - w1 / 2, w1, w1);
			}

			g.setColor(new Color(u.getRGB()));
			g.fillOval((int) p2.x - w2 / 2, (int) p2.y - w2 / 2, w2, w2);

		}

		// for (Projectile p : world.projectiles) {
		// g.setColor(p.c);
		// Point s = screenPos(p.x, p.y, p.z);
		// Point t = screenPos(p.x + p.vx, p.y + p.vy, p.z + p.vz);
		// g.drawLine(s.x, s.y, t.x, t.y);
		// }
		// g.drawImage(graphics.getImage(), 0, 0, this);

		g.setStroke(new BasicStroke(1));

		g.setFont(new Font("SANS_SERIF", Font.PLAIN, 20));

		int count = 0, LIMIT = 4;
		if (menuState == 0)
			for (int i = 0; i < Unit.NUM_ORDERS; i++) {
				if (!notOrders.contains(i)) {
					g.setColor(Methods.randomColor(i, 190));
					g.fillRect(GameDriver.screen.width - 100 * (count / LIMIT + 1), getHeight() - (count % LIMIT + 1)
							* 50, 100, 50);
					g.setColor(Color.BLACK);
					g.drawString(Unit.ORDER_NAMES[i], GameDriver.screen.width - 100 * (count / LIMIT + 1) + 10,
							getHeight() - (count % LIMIT + 1) * 50 + 40);
					g.drawRect(GameDriver.screen.width - 100 * (count / LIMIT + 1), getHeight() - (count % LIMIT + 1)
							* 50, 100, 50);
					count++;
				}
			}
		else if (menuState == -1) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("SANS_SERIF", Font.BOLD + Font.ITALIC, 40));
			g.drawString(Unit.ORDER_NAMES[pendingOrder % 100], GameDriver.screen.width - 200, getHeight() - 60);
			g.setFont(new Font("SANS_SERIF", Font.PLAIN, 20));
		} else if (menuState > 0) {
			ArrayList<Integer> orders = new ArrayList<Integer>();
			for (Unit u : selected)
				for (int i : u.orders)
					if (!orders.contains(i) && i % 100 == menuState)
						orders.add(i);

			for (int i : orders) {
				g.setColor(Methods.randomColor(i, 190));
				g.fillRect(GameDriver.screen.width - 100 * (count / LIMIT + 1), getHeight() - (count % LIMIT + 1) * 50,
						100, 50);
				g.setColor(Color.BLACK);
				g.drawString(i / 100 + "", GameDriver.screen.width - 100 * (count / LIMIT + 1) + 10, getHeight()
						- (count % LIMIT + 1) * 50 + 40);
				g.drawRect(GameDriver.screen.width - 100 * (count / LIMIT + 1), getHeight() - (count % LIMIT + 1) * 50,
						100, 50);
				count++;
			}
		}

		// g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
		// 0.7f));
		g.drawImage(minimap.getImage(), 0, getHeight() - minimap.getSY(), this);
		chat.paintComponent(g);
		// g.setComposite(AlphaComposite.SrcOver);

		g.setFont(new Font("SANS_SERIF", Font.PLAIN, 20));

		if (down.contains(KeyEvent.VK_F1)) {
			g.setColor(options.background);
			g.drawString("" + paintFPS, 21, 51);
			g.drawString("" + drawFPS, 21, 101);
			g.drawString("" + lightFPS, 21, 151);
		}

		g.setColor(options.foreground);
		if (down.contains(KeyEvent.VK_F1)) {
			g.drawString("" + paintFPS, 20, 50);
			g.drawString("" + drawFPS, 20, 100);
			g.drawString("" + lightFPS, 20, 150);
		}

		g.drawString(player.coconuts + " coconuts", 10, getHeight() - minimap.getSY() - 40);
		g.drawString(player.hapiness + " happiness", 10, getHeight() - minimap.getSY() - 10);
		// g.setColor(Color.BLACK);
		// g.drawString("" + player.coconuts, GameDriver.screen.width - 121,
		// 51);
		// g.drawString("" + player.hapiness, GameDriver.screen.width - 121,
		// 101);

		g.setColor(Color.CYAN);
		g.drawLine(retreiveCursorPos().x, retreiveCursorPos().y - options.cursorSize, retreiveCursorPos().x,
				retreiveCursorPos().y + options.cursorSize);
		g.drawLine(retreiveCursorPos().x - options.cursorSize, retreiveCursorPos().y, retreiveCursorPos().x
				+ options.cursorSize, retreiveCursorPos().y);
		// g.fillOval(retreiveCursorPos().x - 10, retreiveCursorPos().y - 10,
		// 20, 20);

	}

	@Override
	public void mouseClicked(MouseEvent evt) {

	}

	@Override
	public void mouseEntered(MouseEvent evt) {
	}

	@Override
	public void mouseExited(MouseEvent evt) {
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		startDrag = new Point(retreiveCursorPos().x, retreiveCursorPos().y);
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (evt.isMetaDown()) {
			for (Unit u : selected) {
				if (Methods.contains(u.orders, 0)) {
					u.perform(0, mx, my);
				}
			}
		}

		else {
			boolean ordered = false;
			if (menuState == -1) {
				for (Unit u : selected) {
					if (Methods.contains(u.orders, pendingOrder)) {
						u.perform(pendingOrder, mx, my);
					}
				}

				menuState = 0;
			} else {
				if (menuState == -999 && retreiveCursorPos().x < minimap.getSX()
						&& retreiveCursorPos().y > getHeight() - minimap.getSY()) {
					minimap.handleRelease(retreiveCursorPos().x, retreiveCursorPos().y
							- (getHeight() - minimap.getSY()));
				}
				if (menuState == 0) {
					menuState = -999;
					// figure out which orders are possible...
					Set<Integer> notOrders = new HashSet<Integer>();
					for (Unit u : selected)
						for (int i = 0; i < Unit.NUM_ORDERS; i++)
							if (!Methods.containsMod(u.orders, i, 100) && !notOrders.contains(i % 100))
								notOrders.add(i % 100);

					// now find which one was selected
					int count = 0, LIMIT = 4, order = -1;
					for (int i = 0; i < Unit.NUM_ORDERS; i++) {
						if (!notOrders.contains(i)) {
							if (new Rectangle(getWidth() - 100 * (count / LIMIT + 1), getHeight() - (count % LIMIT + 1)
									* 50, 100, 50).contains(retreiveCursorPos())) {
								order = i;
								break;
							}
							count++;
						}
					}
					if (order > 0) {
						ordered = true;
						doSomethingAtMenu0(order);
					}
				} else if (menuState > 0) {
					ArrayList<Integer> orders = new ArrayList<Integer>();
					for (Unit u : selected)
						for (int i : u.orders)
							if (!orders.contains(i) && i % 100 == menuState)
								orders.add(i);

					int count = 0, LIMIT = 4, order = -1;
					for (int i : orders) {
						if (new Rectangle(getWidth() - 100 * (count / LIMIT + 1), getHeight() - (count % LIMIT + 1)
								* 50, 100, 50).contains(retreiveCursorPos())) {
							order = i;
							break;
						}
						count++;
					}

					if (order > 0) {
						ordered = true;
						doSomethingAtMenuMoreThan0(order);
					}
				}
				if (!ordered) {
					// for selecting units (on mouseRelease)
					if (!down.contains(KeyEvent.VK_SHIFT))
						selected.clear();
					for (Unit u : player.units) {
						Vector2D p = screenPos(u.x, u.y, u.z);

						if (startDrag != null && p.x < Math.max(startDrag.x, retreiveCursorPos().x)
								&& p.x > Math.min(startDrag.x, retreiveCursorPos().x)
								&& p.y < Math.max(retreiveCursorPos().y, startDrag.y)
								&& p.y > Math.min(retreiveCursorPos().y, startDrag.y)) {
							selected.add(u);
							menuState = 0;
						}
					}
				}
			}
		}

		startDrag = null;
		endDrag = null;
	}

	public void doSomethingAtMenuMoreThan0(int order) {
		if (order >= 0) {
			if (Unit.needsPoint(order)) {
				menuState = -1;
				pendingOrder = order;
			} else
				for (Unit u : selected)
					if (Methods.contains(u.orders, order))
						u.perform(order, 0, 0);
		}
	}

	/**
	 * @param order
	 *        the type of order (0-9 currently, but more formally
	 *        0-Unit.NUM_ORDERS) DOES NOT TAKE SPECIFIC ORDERS such as
	 *        "BUILD CATAPULUT" but only more general ones such as "BUILD"
	 */
	public void doSomethingAtMenu0(int order) {
		// System.out.println(Unit.ORDER_NAMES[order] + "(" + order +
		// ") order recieved ...\n\tMenu? "
		// + Unit.needsMenu(order) + "\n\tPoint? " + Unit.needsPoint(order) +
		// "\n");
		if (order >= 0) {
			if (Unit.needsMenu(order))
				menuState = order;// safe because move (0) doesn't require a
			// menu
			else if (Unit.needsPoint(order)) {
				menuState = -1;
				pendingOrder = order;
			} else
				for (Unit u : selected)
					if (Methods.contains(u.orders, order))
						u.perform(order, 0, 0);
		}
	}

	public void updateCursorPos(Point p) {
		if (options.cursor) {
			cursor.x = Math.min(Math.max(retreiveCursorPos().x + p.x - GameDriver.screen.width / 2, 0),
					GameDriver.screen.width - 1);
			cursor.y = Math.min(Math.max(retreiveCursorPos().y + p.y - GameDriver.screen.height / 2, 0),
					GameDriver.screen.height - 1);
			robot.mouseMove(GameDriver.screen.width / 2, GameDriver.screen.height / 2);
		}
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		updateCursorPos(evt.getPoint());
		if (menuState == -999 && retreiveCursorPos().x < minimap.getSX()
				&& retreiveCursorPos().y > getHeight() - minimap.getSY()) {
			minimap.handleRelease(retreiveCursorPos().x, retreiveCursorPos().y - (getHeight() - minimap.getSY()));
		}
		endDrag = new Point(retreiveCursorPos().x, retreiveCursorPos().y);
	}

	@Override
	public void mouseMoved(MouseEvent evt) {
		updateCursorPos(evt.getPoint());
	}

	private Point retreiveCursorPos() {
		return (options.cursor ? cursor : Methods.mouse());
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		down.add(evt.getKeyCode());

		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			GameWindow.self.bot.setVisible(!GameWindow.self.bot.isVisible());
			GameWindow.self.enter.requestFocus();
		}
	}

	@Override
	public void keyReleased(KeyEvent evt) {
		down.remove(evt.getKeyCode());

		if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
			selected.clear();
			menuState = -999;
		}

		if (menuState == 0) {
			for (int i = 0; i < Unit.NUM_ORDERS; i++)
				if (evt.getKeyCode() == Unit.ORDER_HOTKEYS[i] && union(selected).contains(i))
					doSomethingAtMenu0(i);
		} else if (menuState > 0) {
			ArrayList<Integer> orders = new ArrayList<Integer>();
			for (Unit u : selected)
				for (int i : u.orders)
					if (!orders.contains(i) && i % 100 == menuState)
						orders.add(i);

			for (int i : orders)
				if (KeyEvent.getKeyText(evt.getKeyCode()).equals("" + (i / 100)))
					doSomethingAtMenuMoreThan0(i);

		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

	public void mouseWheelMoved(MouseWheelEvent evt) {
		distance += evt.getWheelRotation();
	}

	public static ArrayList<Integer> union(Collection<? extends Unit> set) {
		ArrayList<Integer> ints = new ArrayList<Integer>();

		for (Unit u : set) {
			for (int o : u.orders) {
				if (!ints.contains(o))
					ints.add(o);
			}
		}

		return ints;
	}

	public static ArrayList<Integer> intersection(Collection<? extends Unit> set) {
		ArrayList<Integer> ints = new ArrayList<Integer>();

		for (int i = 0; i < Unit.NUM_ORDERS; i++)
			ints.add(i);

		for (Unit u : set) {
			for (int i = 0; i < Unit.NUM_ORDERS; i++)
				if (!Methods.containsMod(u.orders, i, 100))
					ints.remove(new Integer(i));
		}

		return ints;
	}

	public static void main(String[] args) {
		GameDriver.main(args);
	}
}
