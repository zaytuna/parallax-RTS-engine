package gui;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import framework.Terrain;
import framework.Unit;
import framework.World;

public class MiniMap {
	BufferedImage image, im2;
	World w;
	GamePanel p;
	private boolean clock = true;
	private int sizeX = 190, sizeY = 280;

	public MiniMap(GamePanel p, World w) {
		w.terrain.createImage(GameDriver.screen.width * 2, GameDriver.screen.height * 2);

		this.w = w;
		this.p = p;
	}

	public int getSX() {
		return sizeX;
	}

	public int getSY() {
		return sizeY;
	}

	public void setSX(int x) {
		sizeX = x;

	}

	public void setSY(int y) {
		sizeY = y;
	}

	public void handleRelease(int x, int y) {
		p.perspectiveX = (Terrain.GRAIN * x * p.world.terrain.getWidth()) / sizeX;
		p.perspectiveY = (Terrain.GRAIN * y * p.world.terrain.getHeight()) / sizeY;
	}

	public void setSize(int s) {
		setSX(s);
		setSY(s);
	}

	public void updateImage() {
		clock = !clock;

		if (clock)
			image = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_ARGB);
		else
			im2 = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = (Graphics2D) (clock ? image : im2).getGraphics();
		g.drawImage(w.terrain.getImage(), 0, 0, sizeX, sizeY, p);
		if (RuntimeOptions.options.fogOWar)
			g.drawImage(p.player.va.getMask(), 0, 0, sizeX, sizeY, p);

		g.setStroke(new BasicStroke(2));

		for (Unit u : w.units) {
			g.setColor(u.owner.color);
			g.fillRect((int) (sizeX * u.x / w.terrain.getWidth()) - 3 * sizeX / w.terrain.getWidth(), (int) (sizeY
					* u.y / w.terrain.getHeight())
					- 3 * sizeY / w.terrain.getHeight(), 1 + 6 * sizeX / w.terrain.getWidth(), 1 + 6 * sizeY
					/ w.terrain.getHeight());
			// g.setColor(new Color(u.getRGB()));
			// g.drawOval((int) (sizeX * u.x / Terrain.GRAIN /
			// w.terrain.getWidth()) - 4 * sizeX / w.terrain.getWidth(),
			// (int) (sizeY * u.y / Terrain.GRAIN / w.terrain.getHeight()) - 4 *
			// sizeY / w.terrain.getHeight(), 1
			// + 8 * sizeX / w.terrain.getWidth(), 1 + 8 * sizeY /
			// w.terrain.getHeight());
		}

		// g.setColor(Color.BLACK);
		// g.drawRect((int) (sizeX * p.perspectiveX / (Terrain.GRAIN *
		// w.terrain.getWidth())), (int) (sizeY
		// * p.perspectiveY / (Terrain.GRAIN * w.terrain.getHeight())), (int)
		// (100 * p.distance / GamePanel.K),
		// (int) (100 * p.distance / GamePanel.K));
		// g.setColor(Color.RED);
		// g.fillOval(sizeX * p.perspectiveX / (Terrain.GRAIN *
		// w.terrain.getWidth()) - 5
		// + (int) (50 * p.distance / GamePanel.K), sizeY * p.perspectiveY
		// / (Terrain.GRAIN * w.terrain.getHeight()) - 5 + (int) (50 *
		// p.distance / GamePanel.K), 10, 10);

		g.setStroke(new BasicStroke(5));
		g.setColor(RuntimeOptions.options.foreground);
		g.drawRect(0, 0, sizeX, sizeY);

		g.dispose();
	}

	public Image getImage() {
		return clock ? im2 : image;
	}
}
