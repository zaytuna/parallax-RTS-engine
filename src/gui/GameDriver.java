package gui;

import framework.Unit;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class GameDriver {
	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	public static void main(String[] args) {
		GameWindow gw = GameWindow.self;
		gw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gw.setTitle(">> Cube Wars <<");
		gw.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		gw.setUndecorated(true);
		gw.setVisible(true);

		// Set the blank cursor to the JFrame.
		gw.getContentPane().setCursor(
				Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB),
						new Point(0, 0), "blank cursor"));

		Unit u = Unit.createFromOwner(gw.me, 0);
		u.teleportTo(55, 55);

		Unit u2 = Unit.createFromOwner(gw.me, 1);
		u2.teleportTo(85, 25);

		Unit u3 = Unit.createFromOwner(gw.me, 2);
		u3.teleportTo(25, 85);

		Unit u4 = Unit.createFromOwner(gw.me, 3);
		u4.teleportTo(5, 5);

		gw.display.world.spawnUnit(u);
		gw.display.world.spawnUnit(u2);
		gw.display.world.spawnUnit(u3);
		gw.display.world.spawnUnit(u4);
	}
}
